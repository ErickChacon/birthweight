## study-birthweight Evaluating the Effects of Extreme Hydro-climatic Events on

Birth-weight in Amazonia

## Running new models


- [X] reclassify gestational age 
- [X] alternative models
    - [X] birth-weight
        - [X] without any rainfall effect
        - [X] without deviation from seasonality effects
        - [X] without intense and deficient rainfall effects
        - [X] without extreme rainfall effects
- [X] disentangle paths: pre-term birth and growth rate.
    * [X] full effects of extreme events on birth-weight (and lbw)
        + pathways: gestational age or fetus growth rate
        + response variable: birth-weight (and lbw)
        + control variable: no
    * [X] effects of extreme events through fetus growth rate
        + pathways: fetus growth rate
        + response variable: birth-weight (or lbw)
        + control variable: gestational age
    * [X] effects of extreme events through gestational age
        + pathways: gestational age
        + response variable: pre-term
        + control variable: no
- [ ] sensitivity to prior assumptions
    - [ ] 3 priors
- [ ] MCMC diagnostics (iterations, convergence and mixing). We will use
  traceplots and acf.
    * [ ] full effects of extreme events on birth-weight (and lbw)
    * [ ] effects of extreme events through fetus growth rate
    * [ ] effects of extreme events through gestational age

## Models summaries

- [ ] table of effects and profile comparisons
    - [ ] birth-weight
    - [ ] birth-weight controlling gestational age
    - [ ] low birth-weight
    - [ ] low birth-weight controlling gestational age
    - [ ] prematurity
- [O] figures
    - [X] exposure to extreme rainfall events
    - [X] municipality-scales characteristics
    - [X] age, seasonal river level index, conception date
    - [ ] municipality effects


[old-notes](10-planning/index.md)

- mod-eff-rain-baseline-significant.pdf (E.D.Figure 8)
- mod-eff-rain-baseline-ci.pdf (Figure 4)
- paper-effects-temporal.pdf (E.D.Figure 13)


## Acepted

- [ ] A. Insert 1+ line of text in the results, prior to the first sub-heading.
  Seems an odd request but it easy to do. (LUKE)
- [ ] B. Reduce methods to try and get nearer to 3000 words. Currently 3,456.
  They'll accept a bit over, though. LUKE, then get your feedback. (LUKE)
- [ ] C. Figures. Each one needs saving as a separate file, of an acceptable
  file type and size (see the attachment guidelines in yesterday's email)
  (ERICK)
    -  at least 300 dpi
    -  check https://www.nature.com/documents/NRJs-guide-to-preparing-final-artwork.pdf
- [ ] D. Reduce Extended Data figures to 10 (max). Put any others in
  Supplemental information. (ERICK) Luke's preference for ones to INCLUDE would
  be: 1, 2, 3, 4, 5, 6a-b-c, 8, 9, 11, 13 These all need saving separately
  following the same guidelines as above. i.e. exclude:
  - 7 (map with combinations of extreme events);
  - 10 (map of municipal effects, because they are shown better in the graphs);
  - 12 (Bw by antenatal consulations because not essential for the story and
    instead is more interesting to someone interesting in going into more
    detail);
  - 14 (river level anomalies)
  - what do you think?
  - These ED figures also need adding to the `SI_Inventory`. see yesterday's
    email.
- [ ] E. Care proof-reading of the whole paper, to make sure the numbers all
  match up, plus match to the figures, etc. Plus the mathematical symbology.
  LUKE and ERICK


There are some extra suggestions about putting the source data for figures on fig.share etc. though perhaps not necessary if you are putting all the code on gitlab?


Any thing else? I'm stopping work on Friday so i'd prefer to be able to send this to editor on Friday. Are you able to do these minor things in time?

Maybe we can have a quick chat tomorrow, say 2pm uk time?

### About figures:

- One-column:
    * Width: 8.8 cm
    * Height (maximum):
        + 13cm (< 300 words)
        + 18cm (< 150 words)
        + 22cm (< 50 words)
- Two-columns:
    * Width: 18 cm
    * Height (maximum):
        + 18.5cm (< 300 words)
        + 21cm (< 150 words)
        + 22.5cm (< 50 words)

- Text:
    * Sans-serif typeface: Helvetica or Arial
    * Text size:
        + Maximum: 7pt
        + Minimum: 5pt
- Colour:
    * Original content: RGB colour mode.
    * Other content: CMYK colour mode.

- Permissions:
    * Clarify the source of any image we do not own

- Formats:
    * Vectors:
        + AI
        + APS
        + PDF
    * Bitmapped: at leat 300 dpi
        + BMP
        + GIF
        + GIMP
        + JPG
        + PNG
        + Tex
        + TIFF

## Figures

- [X] Fig 1:
    * family: "helvetica"
    * weight: 14.1 KB
    * width: 18
    * height: 13
    * caption text: < 300 words
    * state: OK

- [ ] Fig 2:
    * family:
    * wieght: 2.7 MB
    * width: 19.1
    * height: 20.6
    * caption text: 
    * state: need reducing size and family font

- [ ] Fig 3:
    * family: "helvetica"
    * weight: 902.2 KB
    * width: 15.5
    * height: 15
    * caption text: < 300 words
    * state:

- [X] Fig 4:
    * family: "helvetica"
    * weight: 10.7 KB
    * width: 18
    * height: 18
    * caption text: < 300 words
    * state: OK

- [X] Fig 5:
    * family: "helvetica"
    * weight: 15.2 KB
    * width: 20
    * height: 12
    * caption text: < 300 words
    * state: OK

- [ ] Fig E.D.1:
    * family: 
    * weight: 1.59MB
    * width: 18.085
    * height: 15.096
    * ppi: 300
    * colour: RGB
    * caption text: < 300 words
    * state: need to reduce size

- [X] Fig E.D.2:
    * family: 
    * weight: 340.9KB
    * width: 8.776
    * height: 5.654
    * ppi: 345
    * colour: RGB
    * caption text: < 300 words
    * state: OK

- [X] Fig E.D.3:
    * family: "helvetica"
    * weight: 196.8 KB
    * width: 18
    * height: 6.2
    * ppi: 350
    * colour: RGB
    * caption text: < 300 words
    * state: OK

- [X] Fig E.D.4:
    * family: 
    * weight: 
    * width: 18.12
    * height: 12.72
    * ppi: 300
    * colour: RGB
    * caption text:
    * state: acceptable (might be problems with text size)

- [X] Fig E.D.5:
    * family: "helvetica"
    * weight: 218.6 KB
    * width: 18
    * height: 5.5
    * ppi: 350
    * colour: RGB
    * caption text: < 300 words
    * state: OK

- [ ] Fig E.D.6:
    * family: "helvetica"
    * weight: 
    * width: 18
    * height: 12.52
    * ppi: 350
    * colour: RGB
    * caption text: < 300 words
    * state: OK

- [ ] Fig E.D.7:
    * family: 
    * weight:
    * width: 13.95
    * height: 10.6
    * ppi: 300
    * colour: RGB
    * caption text:
    * state: possibly ok

- [X] Fig E.D.8:
    * family: "helvetica"
    * weight: 681.7 KB
    * width: 18
    * height: 21
    * ppi: 350
    * colour: RGB
    * caption text: < 150 words
    * state: possibly ok

- [X] Fig E.D.9:
    * family: "helvetica"
    * weight: 250.9 KB
    * width: 18
    * height: 11.7
    * ppi: 350
    * colour: RGB
    * caption text:
    * state: OK

- [X] Fig E.D.10:
    * family: "helvetica"
    * weight: 
    * width: 18
    * height: 17.4
    * ppi: 350
    * colour: RGB
    * caption text: < 150 words
    * state: OK

- [X] Fig E.D.11:
    * family: "helvetica"
    * weight: 387.4 KB
    * width: 18
    * height: 11.7
    * ppi: 350
    * colour: RGB
    * caption text:
    * state: OK

- [X] Fig E.D.12:
    * family: "helvetica"
    * weight: 432.9 KB
    * width: 18
    * height: 18
    * ppi: 350
    * colour: RGB
    * caption text:
    * state: OK


- [X] Fig E.D.13:
    * family: "helvetica"
    * weight: 163.8 KB
    * width: 18
    * height: 6.3
    * ppi: 350
    * colour: RGB
    * caption text:
    * state: OK

- [ ] Fig E.D.14:
    * family: 
    * weight:
    * width: 13.95
    * height: 10.6
    * ppi: 300
    * colour: RGB
    * caption text:
    * state: not sure

- [ ] Table E.D.1.

---
title: "Linear model with random effects: burn-in and thinning"
prerequisites:
    - data/modelled/bw-muni-08-lm-re-rectified.rds
targets:
    - data/modelled/bw-muni-08-lm-re-burned.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

model_file <- file.path(path_modelled, "bw-muni-08-lm-re.rds")
model_file_rectified <- gsub("(\\.rds)$", "-rectified\\1", model_file)
model_file_burned <- gsub("(\\.rds)$", "-burned\\1", model_file)

model <- readRDS(model_file_rectified)
```

## Burn-in and thinning

```{r}
model$samples <- window(model$samples, start = 100, thin = 5)
system.time(saveRDS(model, file = model_file_burned))
```

## Maximum auto-correlation function (ACF)

```{r maximum-acf, fig.cap = "Maximum ACF of samples for $\\mu$ (left) and $\\sigma$ (right)"}
par(mar = c(4, 4, 0.5, 0), mfrow = c(1, 2))
plot(model, model = "mu", which = "max-acf", spar = FALSE)
plot(model, model = "sigma", which = "max-acf", ylab = "")
```

## MCMC convergence

```{r samples, fig.width = 7, fig.height = 3.5}
par(mar = c(4, 4, 3, 1), mfrow = c(1, 2))
plot(model, which = "samples")
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

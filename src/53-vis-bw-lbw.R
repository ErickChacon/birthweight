

gg_rain <- function (object, model, term, grid = 100, FUN = c95_expression, intercept = FALSE,
                      trans = 'identity',
                      base_size = 7,
                      theme = ggplot2::theme_bw(base_size, base_family = "Helvetica"),
                      data_points = NULL, x_breaks = NULL, weeks_factor = 2.2,
                      labs = c('Exposure to extreme deficient rainfall',
                               'Exposure to extreme intense rainfall'),
                      labs_weeks = c('Weeks exposed to extreme deficient rainfall',
                                     'Weeks exposed to extreme intense rainfall'),
                      labs_ci = c("Prenatal exposure to extreme rainfall events",
                                  "Birthweight effects in grams"),
                     model.frame = object$model.frame, baseline = FALSE,
                     optional = c("significant", "ci", "weeks"),
                     reference = 0, which_shade = NULL, ...) {

    output <- list()

    # EFFECTS: QUANTILES
    if (baseline) {
        baseline_profile <- as.numeric(data_points[1, 1:2])
        predict_long <- predict_grid_baseline(
            object, model, term, grid, baseline_profile, FUN = FUN, intercept = intercept,
            model.frame = model.frame)
    } else {
        predict_long <- predict_grid(object, model, term, grid, FUN = FUN,
                                     intercept = intercept, model.frame)
    }
    predict_long$significant <-
        sign(predict_long[, 3] - reference) == sign(predict_long[, 5] - reference)
    predict_long <- pred_grid_to_long(predict_long, c(1:2, 6))
    gg_effect <- ggplot_3d_bamlss(predict_long, trans = trans, ...)

    if (!is.null(x_breaks)) {
        gg_effect <- gg_effect + scale_x_continuous(expand = c(0, 0), breaks = x_breaks)
    }
    gg_effect <- gg_effect +
        labs(x = labs[1], y = labs[2])

    output$quantiles <- gg_effect

    # EFFECTS: SIGNIFICANT SHADED
    if ("significant" %in% optional) {
        if (is.null(which_shade)) {
            which_shade <- floor((length(FUN(1:10)) + 1) / 2)
        }

        predict_long_mean <- subset(predict_long, as.integer(statistic) == which_shade)
        gg_effect_mean <- ggplot_3d_bamlss(predict_long_mean, trans = trans,
                                           only_significant = TRUE, ...)

        if (!is.null(x_breaks)) {
            gg_effect_mean <- gg_effect_mean +
                scale_x_continuous(expand = c(0, 0), breaks = x_breaks)
        }
        gg_effect_mean <- gg_effect_mean +
            labs(x = labs[1], y = labs[2])

        output$significant <- gg_effect_mean
    }

    # ADD POINTS TO EFFECTS AND CREDIBLE INTERVALS
    if (!is.null(data_points)) {
        varnames <- term_vars(term)
        names(data_points)[1:2] <- varnames

        add_points <- function(x) {
            x +
                geom_point(data = data_points, size = rel(0.6)) +
                geom_text(aes_string(x = varnames[1], y = varnames[2], label = 'label',
                                     hjust = 'hjust', vjust = 'vjust'),
                          data_points, size = base_size / ggplot2::.pt,
                          fontface = "bold", check_overlap = TRUE)
        }

        out_which <- intersect(c("quantiles", "significant"), names(output))
        output[out_which] <- lapply(output[out_which], add_points)
    }

    if (!is.null(data_points) && "ci" %in% optional) {

        # CREDIBLE INTERVALS FOR SCENARIOS
        if (baseline) {
            compare <- predict(object, data_points, model, term, FUN = identity,
                               intercept = intercept)
            compare <- as.matrix(compare)
            compare <- t(apply(t(t(compare) - compare[1, ]), 1, FUN))
            data_points[c("lower", "mean", "upper")] <- get(trans)(compare)
            data_points <- subset(data_points, label != "A")
        } else {
            data_points[c("lower", "mean", "upper")] <-
                get(trans)(predict(object, data_points, model, term, FUN = FUN,
                                   intercept = intercept))
        }

        output$ci_data <- data_points

        gg_effect_ci <- ggplot(data_points, aes(description, mean)) +
            geom_errorbar(aes(ymin = lower, ymax = upper), size = rel(0.4), width = 0.25) +
            geom_point(col = 2, size = rel(2)) +
            geom_hline(yintercept = get(trans)(reference), lty = 2, col = 2) +
            labs(x = labs_ci[1], y = labs_ci[2]) +
            theme
        if (trans == 'log') {
            gg_effect_ci <- gg_effect_ci +
                scale_y_continuous(labels = function(x) round_odds(exp(x)))
        }

        output$ci <- gg_effect_ci
    }

    if ("weeks" %in% optional) {
        to_weeks <- function(x) x * 100 / weeks_factor * sign(mean(x, na.rm = TRUE))
        to_weeks_inv <- function(x) x * weeks_factor / 100
        gg_weeks <- function(x) {
            x +
                scale_x_continuous(trans = "reverse", expand = c(0, 0),
                                   labels = function(x) paste0(to_weeks(x), "%"),
                                   breaks = - 1 * to_weeks_inv(seq(0, 100, 10))) +
                scale_y_continuous(expand = c(0, 0),
                                   labels = function(x) paste0(to_weeks(x), "%"),
                                   breaks = to_weeks_inv(seq(0, 100, 10))) +
                labs(x = labs_weeks[1], y = labs_weeks[2])
        }

        out_which <- intersect(c("quantiles", "significant"), names(output))
        out_new <- paste0(out_which, "_weeks")
        output[out_new] <- lapply(output[out_which], gg_weeks)
    }

    return(output)
}


# pr <- predict_grid_long(model_bw, "mu", "s(neg_ext_mbsi_mean_8wk,pos_ext_mbsi_mean_8wk)",
#                         20, intercept = FALSE,
#                         FUN = function (x) {c95_expression(x, prefix = letters[4:6])})
# ba <- ggplot_3d_bamlss(pr, 's(neg_ext_mbsi_mean_8wk,pos_ext_mbsi_mean_8wk)', binwidth = 70)
# ba <- vis_ext3(model_bw, 'mu', 's(neg_ext_mbsi_mean_8wk,pos_ext_mbsi_mean_8wk)', grid = 20,
#          FUN = c95_expression, binwidth = 70, data_points = data_points)
# ggsave('ba.pdf', ba, width = 20, height = 6.7, units = "cm")
# ba <- vis_ext3(model_lbw, 'pi', 's(neg_ext_mbsi_mean_8wk,pos_ext_mbsi_mean_8wk)', grid = 80,
#           FUN = function (x) c95_expression(exp(x), 'odds'), trans = 'log', binwidth = 1, data_points = data_points, rev = TRUE)
# ggsave('ba.pdf', ba, width = 20, height = 6.7, units = "cm")



visualize_extremes <- function (object, model, grid = 120, FUN = c95,
        trans = "identity", bw = c(100, 10, 10),
        breaks = list(c(0, 0.25, 1, 10, 100, 600),
                      c(0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75),
                      unique(c(seq(0.7, 1, 0.05), seq(1, 1.8, 0.2))))) {

    stat_labels <- c(expression(paste("d) Lower quantile of the effect: ", Q[0.025], "(x)")),
                     expression(paste("e) Mean of the effect: ", E,"(x)")),
                     expression(paste("f) Upper quantile of the effect: ", Q[0.975], "(x)")))

    if (model == "pi") {
        FUN <- function (x) c95(exp(x))
        trans <- "log"
        stat_labels <- c(expression(paste("a) Lower quantile of the odds: ", Q[0.025], "(exp(x))")),
                     expression(paste("b) Mean of the effect: ", E,"(x)")),
                     expression(paste("c) Upper quantile of the effect: ", Q[0.975], "(x)")))
    } else {
    }

    # extreme events effects

    term_extremes <- "s(neg_ext_mbsi_mean_8wk,pos_ext_mbsi_mean_8wk)"
    predict_long <- predict_grid_long(object, bwdata_model, model, term_extremes,
                                      grid, FUN = FUN, intercept = FALSE)

    situations <- c("None",
                    "Intense and deficient",
                    "Intense",
                    "Deficient")
    situations <- sapply(situations, function(x) day2day::wrapit(x, 25))
    # data_points <- data.frame(neg_ext_mbsi_mean_8wk = c(-0.08, -0.55, -0.02, -1),
    #                           pos_ext_mbsi_mean_8wk = c(0.08, 0.34, 0.77, 0.03),
    #                           label = c("A", "B", "C", "D"),
    #                           description = factor(situations, levels = situations))
    data_points <- data.frame(neg_ext_mbsi_mean_8wk = c(-0.08, -0.54, -0.02, -1.03),
                              pos_ext_mbsi_mean_8wk = c(0.08, 0.4, 0.8, 0.03),
                              label = c("A", "B", "C", "D"),
                              description = factor(situations, levels = situations))

    if (model == "pi") {
        gg_effect <- ggplot_3d_bamlss(predict_long, term_extremes, trans = trans, breaks = breaks[[1]])
    } else {
        gg_effect <- ggplot_3d_bamlss(predict_long, term_extremes, trans = trans, bw = bw[[1]])
    }

    gg_effect <- gg_effect +
        scale_x_continuous(expand = c(0, 0), breaks = seq(-2, 0, 0.2)) +
        labs(x = "Exposure to extreme deficient rainfall",
             y = "Exposure to extreme intense rainfall") +
        geom_text(aes(x = neg_ext_mbsi_mean_8wk, y = pos_ext_mbsi_mean_8wk, label = label), data_points,
                  size = rel(4), fontface = "bold")
    # ggsave(file.path(path_save, paste0(model, "_eff_extremes_exposure.pdf")),
    #        gg_effect, width = 20, height = 6.7, units = "cm")

    # credible intervals for 4 scenarios

    data_points[c("lower", "mean", "upper")] <-
      predict(object, data_points, model, term_extremes, FUN = FUN, intercept = FALSE)

    gg_effect_ci <- ggplot(data_points, aes(description, mean)) +
      geom_errorbar(aes(ymin = lower, ymax = upper), size = rel(0.4), width = 0.25) +
      geom_point(col = 2, size = rel(2)) +
      labs(x = "Prenatal exposure to extreme rainfall events",
           y = "Birthweight Effects in Grams")

    if (model == "pi") {
        gg_effect_ci <- gg_effect_ci +
            scale_y_continuous(trans = trans, breaks = c(0, 1, 10, 100, 1000)) +
            labs(x = "Prenatal exposure to extreme rainfall events",
                 y = "Odds of low birthweight")
    }
    # ggsave(file.path(path_save, paste0(model, "_eff_extremes_exposure_ci.pdf")),
    #        gg_effect_ci, width = 6.3, height = 3.5, units = "in")

    #  weeks conversion

    predict_long_weeks <- predict_long %>%
         mutate_at(vars(1:2), ~ . * 100 / 2.2 * sign(mean(.)))

    data_points_weeks <- data_points %>%
         mutate_at(vars(1:2), ~ . * 100 / 2.2 * sign(mean(.)))

  # neg_ext_mbsi_mean_8wk pos_ext_mbsi_mean_8wk label           description
  # 1             3.6363636              3.636364     A                  None
  # 2            24.5454545             18.181818     B Intense and deficient
  # 3             0.9090909             36.363636     C               Intense
  # 4            46.8181818              1.363636     D             Deficient

    if (model == "pi") {
        gg_effect_weeks <- ggplot_3d_bamlss(predict_long_weeks, term_extremes, trans = trans, breaks = breaks[[1]])
    } else {
        gg_effect_weeks <- ggplot_3d_bamlss(predict_long_weeks, term_extremes, trans = trans, bw = bw[[1]])
    }
    gg_effect_weeks <- gg_effect_weeks +
        scale_x_continuous(expand = c(0, 0), breaks = seq(0, 100, 10),
                           labels = function (x) paste0(x, "%")) +
        scale_y_continuous(expand = c(0, 0), breaks = seq(0, 100, 10),
                           labels = function (x) paste0(x, "%")) +
        labs(x = "Weeks exposed to extreme deficient rainfall",
             y = "Weeks exposed to extreme intense rainfall") +
        geom_text(aes(x = neg_ext_mbsi_mean_8wk, y = pos_ext_mbsi_mean_8wk, label = label), data_points_weeks,
                  size = rel(4), fontface = "bold")
    # ggsave(file.path(path_save, paste0(model, "_eff_extremes_exposure_weeks.pdf")),
    #        gg_effect_weeks, width = 20, height = 6.7, units = "cm")

    # floods and droughts

    term_mckee <- "s(neg_mckee_mean_8wk,pos_mckee_mean_8wk)"
    term_mckee_vars <- term_vars(term_mckee)
    predict_long_mckee <- predict_grid_long(object, bwdata_model, model, term_mckee,
                                      grid, FUN = FUN, intercept = FALSE)

    if (model == "pi") {
        gg_effect_mckee <- ggplot_3d_bamlss(predict_long_mckee, term_mckee, trans = trans, breaks = breaks[[2]])
    } else {
        gg_effect_mckee <- ggplot_3d_bamlss(predict_long_mckee, term_mckee, trans = trans, bw = bw[[2]])
    }
    gg_effect_mckee <- gg_effect_mckee +
        scale_x_continuous(expand = c(0, 0), breaks = - seq(0, 2, 0.3)) +
        scale_y_continuous(expand = c(0, 0), breaks = seq(0, 2, 0.2)) +
        labs(x = "Exposure to non-extreme deficient rainfall",
             y = "Exposure to non-extreme intense rainfall")
    # ggsave(file.path(path_save, paste0(model, "_eff_mckee_exposure.pdf")),
    #        gg_effect_mckee, width = 20, height = 6.7, units = "cm")

    # deviation from seasonality

    term_devs <- "s(neg_mbsi_mean_1wk,pos_mbsi_mean_1wk)"
    term_devs_vars <- term_vars(term_devs)
    predict_long_devs <- predict_grid_long(object, bwdata_model, model, term_devs,
                                      grid, FUN = FUN, intercept = FALSE)

    if (model == "pi") {
        gg_effect_devs <- ggplot_3d_bamlss(predict_long_devs, term_devs, trans = trans, breaks = breaks[[3]])
    } else {
        gg_effect_devs <- ggplot_3d_bamlss(predict_long_devs, term_devs, trans = trans, bw = bw[[3]])
    }
    gg_effect_devs <- gg_effect_devs +
        scale_x_continuous(expand = c(0, 0), breaks = - seq(0, 2, 0.2)) +
        scale_y_continuous(expand = c(0, 0), breaks = seq(0, 2, 0.2)) +
        labs(x = "Negative deviation from seasonality",
             y = "Positive deviation from seasonality")
    # ggsave(file.path(path_save, paste0(model, "_eff_seasondev_exposure.pdf")),
    #        gg_effect_devs, width = 20, height = 6.7, units = "cm")

    return(list(gg_effect))
    # return(list(gg_effect, gg_effect_ci, gg_effect_weeks, gg_effect_mckee, gg_effect_devs))

}


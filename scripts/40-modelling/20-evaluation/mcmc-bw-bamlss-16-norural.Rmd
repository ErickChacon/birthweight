---
title: "Evaluate MCMC convergence of main model"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_output <- file.path(path_proj, "60-outputs")
path_out_model <- file.path(path_output, "40-modelling", "10-fitting")

model_file <- file.path(path_out_model, "bw-bamlss-16-norural.RData")
model_name <- load(model_file)
```

## Maximum ACF

```{r}
model_file_aux <- gsub("10-fitting", "20-evaluation", model_file)
acf_file <- gsub("\\.RData$", "-macf-%03d\\.png", model_file_aux)
png(acf_file, width = 480 * 3, height = 480 * 3, pointsize = 30)
plot(get(model_name), model = "mu", which = "max-acf")
plot(get(model_name), model = "sigma", which = "max-acf")
dev.off()
```

## Check MCMC convergence

```{r}
mcmc_file <- gsub("\\.RData$", "-samples-%03d\\.png", model_file_aux)
png(mcmc_file, width = 480 * 3, height = 480 * 4, pointsize = 30)
plot(get(model_name), which = "samples")
dev.off()
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

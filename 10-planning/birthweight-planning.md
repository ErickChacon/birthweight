## Todo

# Task to do for birthweight study

## Cleaning and Processing
- [X] Organize cleaning scripts
  - [X] Clean main database.
  - [X] Select remote cities in the Amazonas FU.
    - [X] Create variable for road_connected.
- [X] Create SPI
  - [X] Compute indices per municipality.
  - [X] Check bamlss and gamlss packages.
  - [X] Compute SPI-related variables at municipality level.
  - [X] If time allow, recompute spi with more smoothed terms (cyclic splines).
- [O] Compute exposure to extreme events at mother level.
  - [X] Detect period of pregnancy per each mother.
  - [X] Obtain SPI, mu and sigma per each week of pregnancy.
  - [X] Summarize indices per mother (monthly, trimester, etc).
  - [X] Simulate example to see the best seasonality index.
  - [X] Compute seasonality index (ecdf).
  - [X] Replace NA by 0 on precipitation index, carefully. (not replace)
  - [X] Compute rivweek for conception date.
  - [X] Compute summaries of SPI for all pregnancy period.
    - [X] Average SPI > 0 and Average SPI < 0
    - [X] Weighted average for SPI > 0 or < 0 and wetness.
  - [X] Compute summaries of SPI:season for all pregnancy period.
    - [X] season from -1:1
    - [X] season from 0:1, this option is more interpretable.
  - [X] Compute summaries of season, mean.
  - [X] Compute indicators per trimester.
    - [X] Positive and negative SPI per trimester.
  - [O] Compute indicator for only extreme events.
    - [X] Positive and negative.
    - [X] Per trimester.
    - [X] Understand season indicators.
    - [X] Indicator that measure dry/wet events in dry/wet season without including confounding. (no solution)
  - [O] Recompute spi to detect more adequately flood and droughts.
    - [X] Modify spi function for running mean of precipitation.
    - [X] Check the most recent precipitation date that is needed (49 week).
    - [X] Compute the SPI for the selected scale time.
    - [X] Compute exposure to extreme events, based on SPI, per mother.
  - [X] Add a category for missing values.

## Exploratory Analysis
- [X] Behaviour of seasonal index and rivweek.
- [X] Effects of seasonal index and rivweek on birth weight and lbw.
- [X] Behaviour of SPI.
- [X] Effects of SPI on birth weight and lbw.
- [X] Check the spi of a random city and compare with bw and lbw.
- [X] Relationship between SPI and wetness.
- [X] Plot birth weight, low birth weight, rain, spi per municipality.
- [ ] Check for interaction between main variables.
- [O] Rainfall EDA
  - [X] Rainfall time series by City
  - [X] SPI time series by City
  - [X] Evaluate SPI to detect droughts and events using different time scales.
- [X] Spi for running means of 1, 2, 3 months
- [X] Plot of spi detecting flood and droughts for the upgrade report.
- [.] Temporal EDA
  - [X] Temporal trend by municipality and global.
    - [X] Original rainfall
    - [X] Moving average of rainfall
    - [X] Spi of original rainfall
    - [X] Spi of moving average of rainfall
    - [X] Mean birth weight
    - [X] Quantiles of birth weight
  - [ ] Distribution of covariates (temporal trend?).
  - [ ] Association between covariates.
  - [ ] Association between and birth weight.
  - [ ] Interaction between covariates.
- [ ] Spatial EDA
  - [ ] Spatial trend by month and global.
  - [ ] Distribution of covariates.
- [ ] Spatio-Temporal EDA
  - [ ] Spatio-temporal trend by municipality.

## Methodology
- [X] `Tackling Confounding`
  - [X] Check association between predictors and precipitation week.
  - [X] Confounding between predictors with gestational age and prec week.
  - [X] Predictors independent of gestational age and prec week.
- [X] `Methodological questions`
  - [X] How to include the autocorrelated seasonality effects.
  - [X] It is better to include the initial SPI or categorical SPI.
  - [X] How to include interaction effects between seasonality and SPI.
  - [X] Check correlation between the selected variables.
- [X] `Methodology Implementation`
  - [X] Implementation of random effects, Bayesian and classic approach.
  - [X] Implementation of random effects on stan.
  - [X] Implement model with correlated SPI coefficients (random effects).
  - [X] Implement model with correlated SPI coefficients (splines).
  - [X] Implement model with gam effect for SPI.
  - [X] Implement model with interaction between SPI:SEASON
  - [X] Check gam for spi structure.
  - [X] Write down second level gam model.
  - [X] Efiicient implementation of second level gam model.
  - [X] Geostatisical vs gam models. Stationary and non-stationary.
  - [X] When covariate and GP are non-correlated it works fine, but gam models seems to understimate the variance.

## Modelling, Validation and Diagnostics
- [X] rivweek + sp + temp + s(spi+, spi-)
- [X] rivweek + sp + temp + s(spi_25+, spi_25-)
- [X] ext_pos + ext_neg
- [X] ext_pos + ext_neg by trimester
- [X] ext_pos + ext_neg by season
- [X] run model for first draft.
- [X] NA values (predictors)
- [.] For report
  - [X] Check missing values: parity covariate
  - [ ] Interaction socio-economic:extreme-events
  - [ ] Model adequacy
  - [ ] Quantile modelling
- [X] `Diagnostics`
  - [X] Residuals do not look normal distributed.
  - [X] Spatial variation is mainly covered by the spatial thin plate.
    - [X] Check residuals of non-sp model.
    - [X] Initialize sp-model with non-sp coefficients.
    - [X] Compare geostatical, CAR and GAM models for spatial effects.

## Writing
- [X] Abstract 18 May
- [X] Introduction 13-17 May
- [X] Extreme Events
- [X] Exploratory Analysis 9-10 May
- [X] Methodology 11-13 May
  - [X] GAM
  - [X] STAR
  - [X] Generalized additive models for location, scale and shape.
  - [X] Quantile regression
  - [X] Multilevel modelling.
  - [X] Including effects.
- [X] Results  28-8 May
- [X] Discussion 3-8 May

## Paper

- Introduction: 31/07/17
- Data Description: 02/08/17
- Extreme events: 04/08/17
  - check if mixture models for precipitation
  - read chapter
  - reorganize section
- Analysis: 06/08/17
- Discussion: 07/08/17

## Modelling improvement
- [X] compare models for gam vs gamlss
- [X] hierarchical models and comparison: not supported
- [X] random effects and gaussian process

## Paper Ben and Luke's comments
### Ben
- Details about missingness of the data.
- Details of the aggregated data at municipality level.
- Gam theory and concepts.
- Use longer series to compare the spi and mbspi.
- Probability integral transform plots: is it due to temporal correlation.
- Evaluate our mbsi method on different cities.
### Luke
- Introduction starting with climate change.
- Results and discussion.

## Review of Ben and Luke comments
- what is the effect of extreme events on birthweight? and in grams?



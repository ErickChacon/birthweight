---
title: "Growth model without rainfall: fitting"
prerequisites:
    - data/processed/bwdata_41_model.fst
targets:
    - data/modelled/lbw-25-growth-no-rain.rds
---

Low birthweight model including covariates at municipality level with linear effects.

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

path_modelled_data <- file.path(path_modelled, "lbw-25-growth-no-rain.rds")
path_modelled_sink <- gsub("\\.rds$", "\\.txt", path_modelled_data)
path_modelled_form <- gsub("(\\.rds)$", "-form\\1", path_modelled_data)

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_41_model.fst"))
```

## Define formula for our model

```{r}
form <- lbw ~ sex + born_race + birth_place + gestation_weeks +
    marital_status + study_years + consult_num + s(age) +
    s(wk_ini) + s(rivwk_conception, bs = "cc") +
    remoteness + prop_tap_toilet + s(res_muni, bs = "re")
```

## Run the model of interest and save results

```{r}
{
    sink(path_modelled_sink)
    bamlss_model <- bamlss(
        form, family = "binomial", data = bwdata_model,
        n.iter = 8000, burnin = 0, cores = 4, combine = FALSE, light = TRUE
    )
    sink()
}
readLines(path_modelled_sink)
```

```{r}
system.time(saveRDS(bamlss_model, file = path_modelled_data))
saveRDS(form, file = path_modelled_form)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

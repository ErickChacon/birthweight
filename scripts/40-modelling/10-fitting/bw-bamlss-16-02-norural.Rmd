---
title: "Modelling birth-weight: rainfall events (2) and no gestational weeks"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

In this script we replicate the model of our previous study with extended period of study of pregnancy (2006 to 2017) and rainfall (2002 to 2018). Additionally we add independent random effects with respect to the municipalities.

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_output <- file.path(path_proj, "60-outputs")
path_out_model_fit <- file.path(path_output, "40-modelling", "10-fitting")

path_out_model_data <- file.path(path_out_model_fit, "bw-bamlss-16-02-norural.RData")
path_out_model_sink <- gsub("\\.RData$", "\\.txt", path_out_model_data)

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_01_model.fst"))
```

## Define formula for our model

We first define the basic formula that includes temporal, spatial, individual and municipality level variables. Later we add effects with respect to the indices of exposure to extreme events during pregnancy and 12 previous weeks.
```{r}
form0 <- born_weight ~ sex + born_race + birth_place +
    marital_status + study_years + consult_num + s(age) +
    s(wk_ini) + s(rivwk_conception, bs = "cc") +
    s(longitud, latitud) +
    s(remoteness) + s(rur_prop) + s(prop_tap_toilet)
```

Now we define the same models as in the previous study.
```{r}
# formula for scale
form_sigma <- sigma ~ sex + born_race + birth_place +
    study_years + consult_num + s(age) +
    s(wk_ini) + s(longitud, latitud)

# formula for location
form <- born_weight ~ sex + born_race + birth_place +
    marital_status + study_years + consult_num + s(age) +
    s(wk_ini) + s(rivwk_conception, bs = "cc") +
    s(longitud, latitud) + s(res_muni, bs = "re") +
    s(remoteness) + s(prop_tap_toilet)
form_mu <- update(form, . ~ . +
                s(neg_mckee_mean_8wk, pos_mckee_mean_8wk, k = 100))
```

## Run the model of interest and save results

```{r}
{
    sink(path_out_model_sink)
    bamlss_model <- bamlss(list(form_mu, form_sigma), family = TF, data = bwdata_model, light = TRUE)
    sink()
}
readLines(path_out_model_sink)
```

```{r}
system.time({
save(bamlss_model, file = path_out_model_data)
})
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

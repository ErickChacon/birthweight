---
title: "Prepare birthweight data for modelling"
prerequisites:
    - data/processed/bwdata_31_exposure.fst
targets:
    - data/processed/bwdata_41_model.fst
---



<p>In this script, the data is processed to be used in our models. This last processing is
done to make sure that the data is in adequate format for the model. We select variables
to be included in the models and also we add missing values of factors as a new level.</p>
<div id="load-packages-read-data-and-source-custom-scripts" class="section level2">
<h2>Load packages, read data and source custom scripts</h2>
<p>Paths are defined relative to the git repository location.</p>
<pre class="r"><code>rm(list = ls())
library(dplyr)</code></pre>
<pre><code>#&gt; 
#&gt; Attaching package: &#39;dplyr&#39;</code></pre>
<pre><code>#&gt; The following objects are masked from &#39;package:stats&#39;:
#&gt; 
#&gt;     filter, lag</code></pre>
<pre><code>#&gt; The following objects are masked from &#39;package:base&#39;:
#&gt; 
#&gt;     intersect, setdiff, setequal, union</code></pre>
<pre class="r"><code>path_proj &lt;- day2day::git_path()
path_data &lt;- file.path(path_proj, &quot;data&quot;)
path_processed &lt;- file.path(path_data, &quot;processed&quot;)

bwdata &lt;- fst::read_fst(file.path(path_processed, &quot;bwdata_31_exposure.fst&quot;)) %&gt;%
    mutate(res_muni = factor(res_muni),
           premature = duration_weeks &lt; 37,
           prop_tap_toilet = prop_tap_toilet / 100
    )</code></pre>
</div>
<div id="standardized-municipality-variables" class="section level2">
<h2>Standardized municipality variables</h2>
<pre class="r"><code>bwdata &lt;- bwdata %&gt;%
    mutate(
        prop_tap_toilet_cd = prop_tap_toilet - mean(prop_tap_toilet),
        remoteness_cd = remoteness - mean(remoteness)
    )</code></pre>
</div>
<div id="relabel-gestational-weeks" class="section level2">
<h2>Relabel gestational weeks</h2>
<p>We relabel the categories of gestational weeks to differentiate between newborns with full
gestational age and lower periods of pregnancy.</p>
<pre class="r"><code>levs &lt;- rev(levels(bwdata$gestation_weeks))
labs &lt;- gsub(&quot;37 - 41|&gt; 42&quot;, &quot;&gt; 37&quot;, levs)
bwdata &lt;- mutate(bwdata, gestation_weeks = factor(gestation_weeks, levs, labs))</code></pre>
</div>
<div id="select-variables-of-interest" class="section level2">
<h2>Select variables of interest</h2>
<p>Selecting the variables that will be used for modelling:</p>
<ul>
<li>birthweight</li>
<li>low birthweight (<span class="math inline">\(&lt; 2500\)</span> g)</li>
<li>prematurity (<span class="math inline">\(&lt; 37\)</span> weeks)</li>
</ul>
<pre class="r"><code>varnames &lt;- c(&quot;born_weight&quot;, &quot;lbw&quot;, &quot;premature&quot;,
              &quot;sex&quot;, &quot;born_race&quot;, &quot;birth_place&quot;, &quot;gestation_weeks&quot;,
              &quot;marital_status&quot;, &quot;study_years&quot;, &quot;consult_num&quot;, &quot;age&quot;,
              &quot;wk_ini&quot;, &quot;rivwk_conception&quot;,
              &quot;res_muni&quot;, &quot;longitud&quot;, &quot;latitud&quot;,
              &quot;remoteness&quot;, &quot;remoteness_cd&quot;, &quot;rur_prop&quot;, &quot;prop_tap_toilet&quot;,
              &quot;prop_tap_toilet_cd&quot;)
bwdata_model &lt;- bwdata %&gt;%
    dplyr::select(dplyr::one_of(varnames), matches(&quot;^(pos|neg)_&quot;))
n_old &lt;- nrow(bwdata_model)</code></pre>
</div>
<div id="add-missing-level-to-factors-with-missing-values" class="section level2">
<h2>Add missing level to factors with missing values</h2>
<p>A missing level is added when:</p>
<ul>
<li>the variable is a factor and</li>
<li>the number of missing values is greater then 100.
This is done because the variable <code>sex</code> is used to model the scale parameter having only one missing value. It made the model non-identifiable and the mcmc samples did not get updated on each iteration.</li>
</ul>
<pre class="r"><code>bwdata_model &lt;- mutate_if(bwdata_model, ~ is.factor(.) &amp; sum(is.na(.)) &gt; 100, addNA)</code></pre>
<p>After this operation, the profiles (rows) with missing values on any of the factor without a <code>NA</code> category will be removed on the next step.</p>
</div>
<div id="remove-missing-values" class="section level2">
<h2>Remove missing values</h2>
<p>Given that we added the <code>NA</code> values as a level in the factors with more than <code>100</code> missing values, this <code>NA</code> level is not considered a missing value on those factors.</p>
<pre class="r"><code>bwdata_model &lt;- na.omit(bwdata_model)
n_new &lt;- nrow(bwdata_model)</code></pre>
<p>Let’s check the number and percentage of removed rows.</p>
<pre class="r"><code>metadata &lt;- c(&quot;Initial number of rows&quot; = n_old,
              &quot;Final number of rows&quot; = n_new,
              &quot;Deleted number of rows&quot; = n_old - n_new)
metadata_perc &lt;- metadata / n_old * 100
cbind(metadata, metadata_perc)</code></pre>
<pre><code>#&gt;                        metadata metadata_perc
#&gt; Initial number of rows   296773    100.000000
#&gt; Final number of rows     291479     98.216145
#&gt; Deleted number of rows     5294      1.783855</code></pre>
</div>
<div id="save-birthweight-data-for-modelling" class="section level2">
<h2>Save birthweight data for modelling</h2>
<pre class="r"><code>fst::write_fst(bwdata_model, path = file.path(path_processed, &quot;bwdata_41_model.fst&quot;))</code></pre>
</div>
<div id="time-to-execute-the-task" class="section level2">
<h2>Time to execute the task</h2>
<p>Only useful when executed with <code>Rscript</code>.</p>
<pre class="r"><code>proc.time()</code></pre>
<pre><code>#&gt;    user  system elapsed 
#&gt;   4.277   0.231   4.524</code></pre>
</div>

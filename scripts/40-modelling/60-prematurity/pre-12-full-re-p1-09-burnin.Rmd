---
title: "Full model with flexible prior: burn-in and thinning"
prerequisites:
    - data/modelled/pre-12-full-re-p1-rectified.rds
targets:
    - data/modelled/pre-12-full-re-p1-burned.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

bwdata_file <- file.path(path_processed, "bwdata_41_model.fst")
path_modelled_data <- file.path(path_modelled, "pre-12-full-re-p1.rds")
path_modelled_rectified <- gsub("(\\.rds)$", "-rectified\\1", path_modelled_data)
path_modelled_burned <- gsub("(\\.rds)$", "-burned\\1", path_modelled_data)
path_form <- gsub("(\\.rds)$", "-form\\1", path_modelled_data)

bwdata_model <- fst::read_fst(bwdata_file)
model <- readRDS(path_modelled_rectified)
form <- readRDS(path_form)
```

## Burn-in and thinning

```{r}
model$samples <- window(model$samples, start = 3001, thin = 20)
```

## Compute results

```{r}
system.time(model$results <- results.bamlss.default(model))
system.time(saveRDS(model, file = path_modelled_burned))
```

## Maximum auto-correlation function (ACF)

```{r maximum-acf, fig.cap = "Maximum ACF of samples for $\\pi$"}
par(mar = c(4, 4, 0.5, 0), mfrow = c(1, 2))
plot(model, model = "pi", which = "max-acf", spar = FALSE)
```

## MCMC convergence

```{r samples}
par(mar = c(4, 4, 3, 1), mfrow = c(1, 2))
plot(model, which = "samples")
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

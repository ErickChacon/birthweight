---
title: "Discrete binning: effects"
prerequisites:
    - data/modelled/bw-08-discrete-bin-burned.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

bwdata_file <- file.path(path_processed, "bwdata_51_test.fst")
model_file <- file.path(path_modelled, "bw-08-discrete-bin.rds")
form_file <- gsub("(\\.rds)$", "-form\\1", model_file)
model_file_burned <- gsub("(\\.rds)$", "-burned\\1", model_file)

bwdata_model <- fst::read_fst(bwdata_file)
form <- readRDS(form_file)
model <- readRDS(model_file_burned)
```

## Compute results

```{r}
model$results <- results.bamlss.default(model)
```

## Fixed effects

```{r}
summary(model)
```

## Smoothed effects

There seems to be a problem with the labels of the random effects plot.

```{r}
par(mar = c(4, 4, 0.5, 0), mfrow = c(1, 2), cex.axis = 0.7)
plot(model, scale = 0, scheme = 2, spar = FALSE)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```


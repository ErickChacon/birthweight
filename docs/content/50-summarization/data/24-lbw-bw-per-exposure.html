---
title: "Visualization of the effects of extreme events using empirical data"
prerequisites:
    - data/processed/bwdata_41_model.fst
targets:
    - data/summarised/paper-data-bw-per-exposure.pdf
    - data/summarised/paper-data-bw-per-exposure.jpg
---



<p>In this script, we compute birth weight (grams) grouped by the number of antenatal
consultations received, and under different levels of exposure to extremely intense or
deficient rainfall events. The resulting figure can be seen at section <a href="#bw-exposure">Visualize birth
weight (grams) and under different levels of exposure</a>. This output
corresponds to <em>Supplementary Figure 3</em> of our paper.</p>
<div id="load-packages-read-data-and-source-custom-scripts" class="section level2">
<h2>Load packages, read data and source custom scripts</h2>
<p>Paths are defined relative to the git repository location.</p>
<pre class="r"><code>rm(list = ls())
library(dplyr)</code></pre>
<pre><code>#&gt; 
#&gt; Attaching package: &#39;dplyr&#39;</code></pre>
<pre><code>#&gt; The following objects are masked from &#39;package:stats&#39;:
#&gt; 
#&gt;     filter, lag</code></pre>
<pre><code>#&gt; The following objects are masked from &#39;package:base&#39;:
#&gt; 
#&gt;     intersect, setdiff, setequal, union</code></pre>
<pre class="r"><code>library(ggplot2)

path_proj &lt;- day2day::git_path()
path_data &lt;- file.path(path_proj, &quot;data&quot;)
path_processed &lt;- file.path(path_data, &quot;processed&quot;)
path_out_sum_data &lt;- file.path(path_proj, &quot;data&quot;, &quot;summarised&quot;)

bwdata_model &lt;- fst::read_fst(file.path(path_processed, &quot;bwdata_41_model.fst&quot;))</code></pre>
</div>
<div id="organize-data-to-make-visualization" class="section level2">
<h2>Organize data to make visualization</h2>
<pre class="r"><code>bwdata_plot &lt;- bwdata_model %&gt;%
    mutate(group1 = cut(neg_ext_mbsi_mean_8wk, c(0.1, -0.154, -0.309, -0.463,
                                                 -0.926, -2)),
           group2 = cut(pos_ext_mbsi_mean_8wk, c(-0.1, 0.119, 0.357, 0.714, 1)),
           group3 = consult_num
    ) %&gt;%
    within({
        levels(group1) &lt;- paste0(c(&quot;(High)&quot;, &quot;(Moderate)&quot;, &quot;(Intermediate)&quot;, &quot;(Low)&quot;, &quot;(None)&quot;),
        &quot;\n exposure to extremely \n deficient events&quot;)
        levels(group2) &lt;- paste0(rev(c(&quot;(High)&quot;, &quot;(Moderate)&quot;, &quot;(Intermediate)&quot;, &quot;(None)&quot;)),
        &quot;\n exposure to extremely \n intense events&quot;)
    })

bwdata_plot &lt;- bwdata_plot %&gt;%
    group_by(group1, group2, group3) %&gt;%
    summarise(n = n(), bw_mean = mean(born_weight), bw_sd = sd(born_weight),
              lbw_mean = mean(lbw), lbw_sd = sqrt(lbw_mean * (1 - lbw_mean))) %&gt;%
    mutate(bw_se = bw_sd / sqrt(n),
           bw_lower = bw_mean - 1.96 * bw_se,
           bw_upper = bw_mean + 1.96 * bw_se,
           lbw_se = lbw_sd / sqrt(n),
           lbw_lower = lbw_mean - 1.96 * lbw_se,
           lbw_upper = lbw_mean + 1.96 * lbw_se
           ) %&gt;%
    tidyr::gather(varname, value, bw_mean:lbw_upper) %&gt;%
    tidyr::extract(varname, c(&quot;type&quot;, &quot;stat&quot;), &quot;([[:alnum:]]+)_([[:alnum:]]+)&quot;) %&gt;%
    tidyr::spread(stat, value) %&gt;%
    mutate(type = factor(type, c(&quot;lbw&quot;, &quot;bw&quot;),
                         c(&quot;Proportion of low birthweight&quot;, &quot;Mean birthweight&quot;)))</code></pre>
<pre><code>#&gt; `summarise()` regrouping output by &#39;group1&#39;, &#39;group2&#39; (override with `.groups` argument)</code></pre>
<pre class="r"><code>bwdata_bw &lt;- bwdata_plot %&gt;%
    filter(type == &quot;Mean birthweight&quot;)</code></pre>
</div>
<div id="create-and-save-figure" class="section level2">
<h2>Create and save figure</h2>
<pre class="r"><code>base_size &lt;- 7
gg1 &lt;- ggplot(subset(bwdata_bw, !is.na(lower))) +
    geom_errorbar(aes(x = group3, ymin = lower, ymax = upper, color = group3),
                  show.legend = FALSE, width = 0.3) +
    geom_point(aes(x = group3, y = mean), col = 1, size = rel(0.4)) +
    facet_grid(group1 ~ group2) +
    geom_hline(yintercept = 3220, col = 1, linetype = 2, size = rel(0.3)) +
    labs(color = NULL,
         x = &quot;Number of antenatal consultations&quot;,
         y = &quot;Birth-weight (grams)&quot;) +
    theme_bw(base_size = base_size, base_family = &quot;Helvetica&quot;) +
    theme(
          panel.grid.minor = element_blank(),
          axis.title = element_text(size = base_size),
          axis.text = element_text(size = base_size),
          strip.text = element_text(size = base_size),
          strip.background = element_rect(fill = &quot;gray96&quot;)
    )

ggsave(file.path(path_out_sum_data, &quot;paper-data-bw-per-exposure.pdf&quot;),
       gg1, width = 18, height = 18, units = &quot;cm&quot;)
ggsave(file.path(path_out_sum_data, &quot;paper-data-bw-per-exposure.jpg&quot;),
       gg1, width = 18, height = 18, units = &quot;cm&quot;, dpi = 350)</code></pre>
</div>
<div id="bw-exposure" class="section level2">
<h2>Visualize birth weight (grams) and under different levels of exposure</h2>
<pre class="r"><code>print(gg1, vp = grid::viewport(gp = grid::gpar(cex = 1.15)))</code></pre>
<p><img src="/birthweight/50-summarization/data/24-lbw-bw-per-exposure_files/figure-html/unnamed-chunk-4-1.png" width="100%" style="display: block; margin: auto;" /></p>
</div>
<div id="time-to-execute-the-task" class="section level2">
<h2>Time to execute the task</h2>
<p>Only useful when executed with <code>Rscript</code>.</p>
<pre class="r"><code>proc.time()</code></pre>
<pre><code>#&gt;    user  system elapsed 
#&gt;  17.547   0.563  18.090</code></pre>
</div>

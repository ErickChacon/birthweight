---
title: "Full model with t-distribution: continue mcmc"
prerequisites:
    - src/43-mcmc-continue-light.R
    - src/45-mcmc-reshape.R
    - src/46-mcmc-last-sample.R
    - data/modelled/bw-10-full-re-t.rds
targets:
    - data/modelled/bw-10-full-re-t-continue.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

source(file.path(path_proj, "src", "43-mcmc-continue-light.R"))
source(file.path(path_proj, "src", "45-mcmc-reshape.R"))
source(file.path(path_proj, "src", "46-mcmc-last-sample.R"))

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_41_model.fst"))

path_modelled_data <- file.path(path_modelled, "bw-10-full-re-t.rds")
path_modelled_form <- gsub("(\\.rds)$", "-form\\1", path_modelled_data)
path_modelled_continue <- gsub("(\\.rds)$", "-continue\\1", path_modelled_data)
path_modelled_sink <- gsub("\\.rds$", "\\.txt", path_modelled_continue)

bamlss_model <- readRDS(path_modelled_data)
form <- readRDS(path_modelled_form)
```

## Continue with sampling

```{r}
starting_value <- last_sample(bamlss_model)
{
    sink(path_modelled_sink)
    bamlss_model <- bamlss(
        form, family = TF, data = bwdata_model,
        start = starting_value, optimizer = FALSE,
        n.iter = 3000, burnin = 0, cores = 4, combine = FALSE, light = TRUE
    )
    sink()
}
readLines(path_modelled_sink)
```

```{r}
system.time(saveRDS(bamlss_model, file = path_modelled_continue))
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

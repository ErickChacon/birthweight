I have compared several model to see how estreme events can be taken into
account as follows:

- Deviations from seasonality
  - Summarized positive and negative spi values for all weeks.
  - Summarized positive and negative spi values until week 25.
  - Conclusion: The summary for all week seems more useful because:
    - Account for more uncertainty and takes into account spi for las trimester.
    - It is more significant.
    - Spi values do not have seasonal structure and does not create confounding with precipitation week on conception week.
- Deviations from seasonality and extreme events.
  - When taking into account both effect, they are not confounded.
  - Deviation from seasonality seems to have a strong spatial structure and its effect changes when including spatial terms but being still significant.
- Extreme values per trimester.
  - Only few points different of zero, making it more over fitted on those values.
- Extreme values per season.
  - Only few points different of zero, easy to overfit.

Given these models, I have decided to include:
- Summary for positive and negative SPIs for all the pregnancy weeks weighted according to gestational age.
- Summary for positive and negative extreme events for all pregnancy weeks.
- Spatial terms.
- Temporal terms.

# what covariates to consider

- One of the important aspects about measuring the impact of extreme events on birthweight is to know which covariates should be included in the model. Assuming that the effect of extreme events could go through intrauterine growth or gestational age, it is important not to control gestational age. This way covariates to be included in the model, should not be associated with gestational age.

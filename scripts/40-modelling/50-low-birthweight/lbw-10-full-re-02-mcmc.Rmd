---
title: "Full model: mcmc"
prerequisites:
    - data/modelled/lbw-10-full-re.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

model_file <- file.path(path_modelled, "lbw-10-full-re.rds")
model <- readRDS(model_file)
```

## Maximum auto-correlation function (ACF)

```{r maximum-acf, fig.cap = "Maximum ACF of samples for $\\pi$"}
par(mar = c(4, 4, 0.5, 0), mfrow = c(1, 2))
plot(model, model = "pi", which = "max-acf", spar = FALSE)
```

## MCMC convergence

```{r samples}
par(mar = c(4, 4, 3, 1), mfrow = c(1, 2))
plot(model, which = "samples")
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

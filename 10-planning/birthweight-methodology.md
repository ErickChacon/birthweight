# Methods for the birthweight study

## Methodology to Identify Extreme Events

## Methodology to Model the Effect of Extreme Events on birthweight

- Variables like remoteness have few unique values on the predictor. Splines defines the number of knots based on the quantiles, then it can be the case that quantiles are not the appropriate way to select the knots for this kind of data.


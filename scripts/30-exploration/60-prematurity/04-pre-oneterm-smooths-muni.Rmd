---
title: "Municipality one-term models: smooths"
prerequisites:
    - data/processed/bwdata_41_model.fst
targets:
    - data/modelled/pre-explore-smooths-muni.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

Simple univariate GAM models to evaluate the relationship prematurity and each covariates.

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(dplyr)
library(mgcv)
library(purrr)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

path_out_model_data <- file.path(path_modelled,  "pre-explore-smooths-muni.rds")

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_41_model.fst"))
```

## Exploratory models of prematurity with municipality covariates

```{r}
terms <- list(
    ~ s(remoteness),
    ~ s(rur_prop),
    ~ s(prop_tap_toilet)
    )

data_model <- tibble::as_tibble(expand.grid(
    formula = map(terms, ~ update(., premature ~ .)),
    gamma = c(1, log(nrow(bwdata_model)), 18, 40)
))

data_model <- data_model %>%
    mutate(model = map2(formula, gamma,
                        ~ gam(.x, "binomial", data = bwdata_model, gamma = .y)))

saveRDS(data_model, file = path_out_model_data)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

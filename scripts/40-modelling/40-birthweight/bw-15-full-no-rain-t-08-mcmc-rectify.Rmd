---
title: "Full model with t-distribution and no rainfall: rectify samples"
prerequisites:
    - data/modelled/bw-15-full-no-rain-t.rds
targets:
    - data/modelled/bw-15-full-no-rain-t-rectified.rds
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 7, fig.height = 3.5)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

source(file.path(path_proj, "src", "41-mcmc-rectify-re.R"))

path_modelled_data <- file.path(path_modelled, "bw-15-full-no-rain-t.rds")
path_modelled_rectified <- gsub("(\\.rds)$", "-rectified\\1", path_modelled_data)

model <- readRDS(path_modelled_data)
```

## Rectify mcmc samples and save

```{r}
model$samples <- rectify_re_list(model$samples, "res_muni", "mu")
model$samples <- rectify_re_list(model$samples, "res_muni", "sigma")

system.time(saveRDS(model, file = path_modelled_rectified))
```

## Maximum auto-correlation function (ACF)

```{r maximum-acf, fig.cap = "Maximum ACF of samples for $\\mu$ (left), $\\sigma$ (center) and $\\nu$(right)", fig.width = 7.5, fig.height = 2.5}
par(mar = c(4, 4, 0.5, 0), mfrow = c(1, 3))
plot(model, model = "mu", which = "max-acf", spar = FALSE)
plot(model, model = "sigma", which = "max-acf", spar = FALSE)
plot(model, model = "nu", which = "max-acf", spar = FALSE)
```

## MCMC convergence

```{r samples}
par(mar = c(4, 4, 3, 1), mfrow = c(1, 2))
plot(model, which = "samples")
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

---
title: "Specific information to be included in the paper"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)
library(dplyr)

path_proj <- day2day::git_path()
pathfi_proj <- dirname(dirname(path_proj))
pathfi_data <- file.path(pathfi_proj, "30-data")
pathfi_processed <- file.path(pathfi_data, "30-processed")
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_output <- file.path(path_proj, "60-outputs")
path_out_model <- file.path(path_output, "40-modelling")

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_01_model.fst"))
# system.time(load(file = file.path(path_out_model, "bamlss_09_bamlss_ire.RData")))
# system.time(load(file = file.path(path_out_model, "bamlss_09_bam_ire_lbw.RData")))

path_src <- file.path(path_proj, "40-src")
source(file.path(path_src, "50-visualize.R"))
```


```{r}

library(ggplot2)

# rur_prop_data <- data.frame(rur_prop = unique(bwdata_model$rur_prop)) %>%
#     cbind(predict_effect(bamlss_09, rur_prop_data, "mu", "s(rur_prop)", intercept = TRUE))
# rur_prop <- ggplot(bwdata_model, aes(rur_prop, born_weight)) +
#     geom_point(aes(rur_prop, Mean), rur_prop_data) +
#     geom_smooth()
# png("rur_prop.png")
# print(rur_prop)
# dev.off()

rur_prop_data <- bwdata_model %>%
    group_by(rur_prop) %>%
    summarise(mean = mean(born_weight), lbwp = mean(born_weight < 2500), logit = psych::logit(lbwp))

ggplot(rur_prop_data, aes(rur_prop, mean)) +
    geom_point() +
    geom_smooth()

ggplot(rur_prop_data, aes(rur_prop, logit)) +
    geom_point() +
    geom_smooth()

sanit_prop_data <- bwdata_model %>%
    group_by(prop_tap_toilet) %>%
    summarise(mean = mean(born_weight), lbwp = mean(born_weight < 2500), logit = psych::logit(lbwp))

ggplot(sanit_prop_data, aes(prop_tap_toilet, mean)) +
    geom_point() +
    geom_smooth()

ggplot(sanit_prop_data, aes(prop_tap_toilet, lbwp)) +
    geom_point() +
    geom_smooth()

remote_prop_data <- bwdata_model %>%
    group_by(remoteness) %>%
    summarise(mean = mean(born_weight), lbwp = mean(born_weight < 2500), logit = psych::logit(lbwp))

ggplot(remote_prop_data, aes(remoteness, mean)) +
    geom_point() +
    geom_smooth()

ggplot(remote_prop_data, aes(remoteness, lbwp)) +
    geom_point() +
    geom_smooth()


```

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

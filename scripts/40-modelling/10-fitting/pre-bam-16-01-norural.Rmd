---
title: "Premature model: with rainfall events"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

Model to identify the effects of extreme hydro-climatic events on prematurity.

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_output <- file.path(path_proj, "60-outputs")
path_out_model_fit <- file.path(path_output, "40-modelling", "10-fitting")

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_01_model.fst"))
```

## Define formula for our model

We first define the basic formula that includes temporal, spatial, individual and municipality level variables. Later we add effects with respect to the indices of exposure to extreme events during pregnancy and 12 previous weeks.

Now we define the same models as in the previous study.
```{r}
# formula for pi
form <- premature ~ sex + born_race + birth_place +
    marital_status + study_years + consult_num + s(age) +
    s(wk_ini) + s(rivwk_conception, bs = "cc") +
    s(longitud, latitud) + s(res_muni, bs = "re") +
    s(remoteness) + s(prop_tap_toilet)
form_pi <- form
```

## Run the model of interest and save results

```{r}
system.time({
bamlss_pre <- bamlss(form_pi, family = "binomial", data = bwdata_model)
})
```

```{r}
system.time({
save(bamlss_pre, file = file.path(path_out_model_fit, "pre-bam-16-01-norural.RData"))
})
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

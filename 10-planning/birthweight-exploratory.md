# Birth weight Exploratory Analysis

## Descriptive Analysis
## Socio-demographic
## Multicollinearity
## Rainfall and anomalies at municipality scale

- *Uncertainty of rainfall*: The data comes from satellite images and we aggrate thos values by municipality. Then the reliability of those values depend on the size of the municipality. For bigger municipalities, the rainfall series is more reliable while the contrary is true for smaller regions.

- *Raw rainfall*: 2009 does not exhibit a tremendous difference in values, but the mean rainfall on the wet season has been more longer and with less uncertainty than other years.

## Seasonality

- Na values per week.
From 41 to 44 weeks theres is a significant difference in the percentage of NA cases, indicating than only a small percentage of mother has a pregnancy period greater than 41 weeks. 38.5 weeks represent 9 months. It indicates two things. First that the mothers with that number of weeks are a error when obtaining the data, likely, due to the ignorance of when that mother was actually pregnant. Indicating that probably those mothers were pregnant before the information they have reported and the rain weeks associated with there pregnancy should be taken 4 weeks before.

- season correlation

As it can be seen, the seasonality index is highly correlated through the weeks of pregnancy. The correlation is practically lost in 12-13 weeks. Then the correlation turns negative having the highest negative correlation around week 26. This negative correlation starts decreasing from then until week 39 and starts to be positive with a high peak around 52 weeks. This indicates that around 13 weeks the correlation reach the maximum and changes its direction.

On the other hand, it also suggest that using two continuous weeks does not provide additional information given that they are highly correlated. Then, choosing only few weeks to includes their sesonality index should be appropiate to model birthweiht. For example, a series of each 13 weeks would be appropiate given that at that moment is when the correlation is lost. It would reduce the seasonality variables to from 57 to 5 weeks.

- season with birthweight.
The plot between birthweight and season for each week, indicates that this relationship is linear, however they are autocorrelated. For Gaussian linear models it should be considered that the effect of each week can be well estimated when including all the precedents weeks.
It can be seen that the effect is positive for the pre-gestational period and the last period. Indicating that those groups who are exposure to more wet periods with have more chance of getting higher birth weight. It could also indicate that the initial (before getting pregnant) and the last periods are the most important for periods with respect to the weight.

What is important to notice is that season has an strong effect in general terms and not for each specific week. Probably, the same happens with the standardized precipitation index, it does not mother if the mother were exposed to one extrema week in a particular season, what mothers is the acumulative effect. For example assuming that there is a hazard function, it would be ideal to obtain an integrated hazard function. It is almost imposible to interpret the results using each week, because they are related, interact with season, etc.

- average season per mother.

Computing the average season per mother produces a perfect covariate to identify low birth weight. However, this summary in influences by the number of weeks than the mother was pregnant. If the number of weeks, pre-term and term, are less than expected, the mean of season indicator is more likely to higher or lower than the average. This behaviour is not desired, the indicator should not be influenced by the number of weeks that mother was exposured.

The differences between cities can be also due to the minimum precipitation, in come municipalities is ts normal to see no rain on the dry season, while in others it is normal that the level of precipitation reduces but not to zero.

## Standardized precipitation Index

The SPI index has a mean around zero for each week, but it has a increasing behaviour with respect to the pregnant week. This is differences of mean results significant, but the size of the effect is trivial, for example 0.02 grams.



## Interaction Seasonality and SPI

## River week, wetness and spi.

- delay in 7 weeks between wetness and river week.
- more benefited those with week 3 of river and week 10 of precipitation.
- is spi associated with prec week?:
  - The values of spi in any week has the same structure en relationship with river week or wetness index due to the same spi values are assigned to different mothers in different weeks of pregnancy.
  - Extreme dry events occurred on dry season (wetness = 0.25) and close to the peak of wet season (wetness = 0.61). With respect to precipitation week, these events occurred around week 15 and 48 respectively. Then, mother who got pregnant during the wet season (precipitation week = 0), would experience the first event on week 15 nd around 5 weeks before getting pregnant for the second event.
- spi positive and negative

## About the possible effect of precipitation on birth weight

It is important to notice that there are some strong differences between the behaviour of precipitation in different cities. IT worth notice that there are some cities where there is no rain in several weeks during the dry season, while there are some municipalities where there is rainfall even in the dry season.

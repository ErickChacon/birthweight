---
title: "Visualization of the replicated bamlss model to evaluate effects of extreme hydro-climatic events on birth-weight in Amazonia"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 10, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

Visualization:

- covariates at individual level
- covariates at municipality level
- spatial effects (using coordinates)
- temporal effects (using week of conception)
- seasonal effects (with respect to river levels)
- three indices of rainfall exposure

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)
library(dplyr)
library(ggplot2)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_src <- file.path(path_proj, "40-src")
path_output <- file.path(path_proj, "60-outputs")
path_out_model <- file.path(path_output, "40-modelling")
path_out_graphs <- file.path(path_output, "60-visualization", "lbw-bw-rural")

dir.create(path_out_graphs, showWarnings = FALSE)

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_01_model.fst"))
model_bw_name <- load(file = file.path(path_out_model, "bamlss_09_bamlss_ire.RData"))
model_lbw_name <- load(file = file.path(path_out_model, "bamlss_09_bam_ire_lbw.RData"))
model_bw <- get(model_bw_name)
model_lbw <- get(model_lbw_name)
rm(list = c(model_bw_name, model_lbw_name))

source(file.path(path_src, "60-visualization.R"))
source(file.path(path_src, "61-bamlss.R"))
```

## Mbsi indices effects

```{r}
grid <- 120

visualize_extremes(model_bw, "mu", path_out_graphs, grid = grid,
                   bw = c(100, 10, 10))

visualize_extremes(model_lbw, "pi", path_out_graphs, grid = grid,
    breaks = list(
                  c(0, 0.25, 1, 10, 100, 600),
                  c(0, 0.25, 0.5, 0.75, 1, 1.25, 1.5, 1.75),
                  unique(c(seq(0.7, 1, 0.1), seq(1, 1.8, 0.2)))
                  ))
```

## non-linear effects municipality level

```{r}

chicol <- "black"

pdf(file.path(path_out_graphs, "mu_eff_remot_sanitation.pdf"), width = 6.3, height = 3)
opar <- par(mar = c(4, 3.5, 0.5, 0.5 - 0.5), mfrow = c(1, 2), cex = 0.6,
            col.lab = chicol, font.lab = 1)
plot(model_bw, model = "mu", term = "s(remoteness)" , scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Mean birth-weight change", side = 2, line = 2.5, col = chicol, font = 1, cex = 0.7)
mtext("Remoteness", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(mar = c(4, 3.5 - 0.5, 0.5, 0.5))
plot(model_bw, model = "mu", term = "s(prop_tap_toilet)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "",
     # ylab = "Mean birth-weight change")
     ylab = "")
mtext("Percentage of population with sanitation", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(opar)
dev.off()

pdf(file.path(path_out_graphs, "pi_eff_remot_sanitation.pdf"), width = 6.3, height = 3)
opar <- par(mar = c(4, 3.5, 0.5, 0.5 - 0.5), mfrow = c(1, 2), cex = 0.6,
            col.lab = chicol, font.lab = 1)
plot(model_lbw, model = "pi", term = "s(remoteness)" , scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Relative Risk", side = 2, line = 2.5, col = chicol, font = 1, cex = 0.7)
mtext("Remoteness", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(mar = c(4, 3.5 - 0.5, 0.5, 0.5))
plot(model_lbw, model = "pi", term = "s(prop_tap_toilet)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "",
     # ylab = "Mean birth-weight change")
     ylab = "")
mtext("Percentage of population with sanitation", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(opar)
dev.off()

```

```{r}

library(lubridate)
rain_date0 <- ymd("2002-01-01") # to 2013-12-31
pdf(file.path(path_out_graphs, "mu_eff_age_season_temp.pdf"), width = 6.3, height = 2.5)
prop <- 0.355
layout(matrix(c(1:3), nrow = 1), widths = c(prop, (1-prop)/2, (1-prop)/2))
opar <- par(mar = c(4, 3.5, 0.5, 0.5), cex = 0.6,
            col.lab = chicol, font.lab = 2)
plot(model_bw, model = "mu", term = "s(age)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Mean birth-weight change", side = 2, line = 2.5, col = chicol, font = 1, cex = 0.7)
mtext("Age", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(opar)
op1 <- par(mar = c(4, 2, 0.5, 0.5), cex = 0.6)
plot(model_bw, model = "mu", term = "s(rivwk_conception)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Seasonal river level index", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
plot(model_bw, model = "mu", term = "s(wk_ini)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "", axes = FALSE)
axis(1,
     at = 1 + floor(difftime(rain_date0 + years(seq(1, 20, 1)), rain_date0, unit = "weeks")),
     labels = 2002 + seq(1, 20, 1))
axis(2, at = seq(-80, 40, by = 20))
mtext("Conception date", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
dev.off()

pdf(file.path(path_out_graphs, "pi_eff_age_season_temp.pdf"), width = 6.3, height = 2.5)
prop <- 0.355
layout(matrix(c(1:3), nrow = 1), widths = c(prop, (1-prop)/2, (1-prop)/2))
opar <- par(mar = c(4, 3.5, 0.5, 0.5), cex = 0.6,
            col.lab = chicol, font.lab = 2)
plot(model_lbw, model = "pi", term = "s(age)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Relative risk", side = 2, line = 2.5, col = chicol, font = 1, cex = 0.7)
mtext("Age", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
par(opar)
op1 <- par(mar = c(4, 2, 0.5, 0.5), cex = 0.6)
plot(model_lbw, model = "pi", term = "s(rivwk_conception)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "")
mtext("Seasonal river level index", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
plot(model_lbw, model = "pi", term = "s(wk_ini)", scheme = 2,
     scale = 0, spar = FALSE, xlab = "", ylab = "", axes = FALSE)
axis(1,
     at = 1 + floor(difftime(rain_date0 + years(seq(1, 20, 1)), rain_date0, unit = "weeks")),
     labels = 2002 + seq(1, 20, 1))
# axis(2, at = seq(-80, 40, by = 10))
axis(2)
mtext("Conception date", side = 1, line = 2.5, col = chicol, font = 1, cex = 0.7)
dev.off()
```


## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

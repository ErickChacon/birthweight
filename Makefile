target_all = docs/content/_index.html \
	docs/content/20-processing/_index.html \
	data/processed/bwdata_41_model.fst \
	docs/content/20-processing/01-bwdata-model.html \
	data/processed/bwdata_51_test.fst \
	docs/content/20-processing/11-bwdata-test.html \
	data/processed/ama_10_penalty.rds \
	docs/content/20-processing/31-ama-penalty.html \
	docs/content/30-exploration/_index.html \
	docs/content/30-exploration/40-birthweight/_index.html \
	data/modelled/bw-explore-smooths.rds \
	docs/content/30-exploration/40-birthweight/02-bw-oneterm-smooths.html \
	docs/content/30-exploration/40-birthweight/03-bw-oneterm-visualize.html \
	data/modelled/bw-explore-smooths-muni.rds \
	docs/content/30-exploration/40-birthweight/04-bw-oneterm-smooths-muni.html \
	docs/content/30-exploration/40-birthweight/05-bw-oneterm-muni-visualize.html \
	docs/content/30-exploration/50-low-birthweight/_index.html \
	data/modelled/lbw-explore-smooths.rds \
	docs/content/30-exploration/50-low-birthweight/02-lbw-oneterm-smooths.html \
	docs/content/30-exploration/50-low-birthweight/03-lbw-oneterm-visualize.html \
	data/modelled/lbw-explore-smooths-muni.rds \
	docs/content/30-exploration/50-low-birthweight/04-lbw-oneterm-smooths-muni.html \
	docs/content/30-exploration/50-low-birthweight/05-lbw-oneterm-muni-visualize.html \
	docs/content/30-exploration/60-prematurity/_index.html \
	data/modelled/pre-explore-smooths.rds \
	docs/content/30-exploration/60-prematurity/02-pre-oneterm-smooths.html \
	docs/content/30-exploration/60-prematurity/03-pre-oneterm-visualize.html \
	data/modelled/pre-explore-smooths-muni.rds \
	docs/content/30-exploration/60-prematurity/04-pre-oneterm-smooths-muni.html \
	docs/content/30-exploration/60-prematurity/05-pre-oneterm-muni-visualize.html \
	docs/content/30-exploration/70-modelling/_index.html \
	docs/content/30-exploration/70-modelling/01-priors.html \
	docs/content/35-model-testing/_index.html \
	docs/content/35-model-testing/10-testing-simulated/_index.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-00-notes.html \
	data/modelled/bw-00-inter.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-02-mcmc.html \
	data/modelled/bw-00-inter-rectified.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-08-mcmc-rectify.html \
	data/modelled/bw-00-inter-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-10-effects.html \
	data/modelled/bw-02-nointer.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-02-mcmc.html \
	data/modelled/bw-02-nointer-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-10-effects.html \
	data/modelled/bw-04-inter-bin.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-02-mcmc.html \
	data/modelled/bw-04-inter-bin-rectified.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-08-mcmc-rectify.html \
	data/modelled/bw-04-inter-bin-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-10-effects.html \
	data/modelled/bw-06-discrete.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-02-mcmc.html \
	data/modelled/bw-06-discrete-rectified.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-08-mcmc-rectify.html \
	data/modelled/bw-06-discrete-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-10-effects.html \
	data/modelled/bw-08-discrete-bin.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-02-mcmc.html \
	data/modelled/bw-08-discrete-bin-rectified.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-08-mcmc-rectify.html \
	data/modelled/bw-08-discrete-bin-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-10-effects.html \
	data/modelled/bw-09-discrete-bin-age.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-02-mcmc.html \
	data/modelled/bw-09-discrete-bin-age-burned.rds \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-10-effects.html \
	docs/content/35-model-testing/20-testing-muni/_index.html \
	data/modelled/bw-muni-08-lm-re-rectified.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-08-mcmc-rectify.html \
	data/modelled/bw-muni-08-lm-re-burned.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-09-burnin.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-10-effects.html \
	data/modelled/bw-muni-10-slm-re.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-02-mcmc.html \
	data/modelled/bw-muni-10-slm-re-rectified.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-08-mcmc-rectify.html \
	data/modelled/bw-muni-10-slm-re-burned.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-09-burnin.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-10-effects.html \
	data/modelled/bw-muni-12-remote-re.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-02-mcmc.html \
	data/modelled/bw-muni-13-remote-re-bin.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-02-mcmc.html \
	data/modelled/bw-muni-14-water-re.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-02-mcmc.html \
	data/modelled/bw-muni-16-sp-re.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-02-mcmc.html \
	data/modelled/bw-muni-17-sp-re-bin.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-02-mcmc.html \
	data/modelled/bw-muni-18-mrf-re.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-02-mcmc.html \
	data/modelled/bw-muni-19-mrf-re-bin.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-02-mcmc.html \
	data/modelled/bw-muni-19-mrf-re-bin-burned.rds \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-09-burnin.html \
	docs/content/40-modelling/_index.html \
	docs/content/40-modelling/40-birthweight/_index.html \
	data/modelled/bw-10-full-re-t.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-02-mcmc.html \
	data/modelled/bw-10-full-re-t-continue.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue.html \
	data/modelled/bw-10-full-re-t-continue2.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue2.html \
	data/modelled/bw-10-full-re-t-merged.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-07-merge.html \
	data/modelled/bw-10-full-re-t-rectified.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-08-mcmc-rectify.html \
	data/modelled/bw-10-full-re-t-burned.rds \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-14-dic.html \
	data/modelled/bw-12-full-re.rds \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-02-mcmc.html \
	data/modelled/bw-12-full-re-rectified.rds \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-08-mcmc-rectify.html \
	data/modelled/bw-12-full-re-burned.rds \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-14-dic.html \
	data/modelled/bw-15-full-no-rain-t.rds \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-02-mcmc.html \
	data/modelled/bw-15-full-no-rain-t-rectified.rds \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-08-mcmc-rectify.html \
	data/modelled/bw-15-full-no-rain-t-burned.rds \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-14-dic.html \
	data/modelled/bw-20-growth-re-t.rds \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-02-mcmc.html \
	data/modelled/bw-20-growth-re-t-continue.rds \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-03-continue.html \
	data/modelled/bw-20-growth-re-t-merged.rds \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-07-merge.html \
	data/modelled/bw-20-growth-re-t-rectified.rds \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-08-mcmc-rectify.html \
	data/modelled/bw-20-growth-re-t-burned.rds \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-14-dic.html \
	data/modelled/bw-25-growth-no-rain-t.rds \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-02-mcmc.html \
	data/modelled/bw-25-growth-no-rain-t-rectified.rds \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-08-mcmc-rectify.html \
	data/modelled/bw-25-growth-no-rain-t-burned.rds \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-14-dic.html \
	docs/content/40-modelling/50-low-birthweight/_index.html \
	data/modelled/lbw-10-full-re.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-03-mcmc-continue.html \
	data/modelled/lbw-10-full-re-merged.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-07-merge.html \
	data/modelled/lbw-10-full-re-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-12-effects-muni.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-14-dic.html \
	data/modelled/lbw-11-full-re-p1.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-02-mcmc.html \
	data/modelled/lbw-11-full-re-p1-rectified.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-08-mcmc-rectify.html \
	data/modelled/lbw-11-full-re-p1-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-10-effects.html \
	data/modelled/lbw-12-full-re-p2.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-02-mcmc.html \
	data/modelled/lbw-12-full-re-p2-rectified.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-08-mcmc-rectify.html \
	data/modelled/lbw-12-full-re-p2-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-10-effects.html \
	data/modelled/lbw-15-full-no-rain.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-02-mcmc.html \
	data/modelled/lbw-15-full-no-rain-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-14-dic.html \
	data/modelled/lbw-20-growth-re.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-02-mcmc.html \
	data/modelled/lbw-20-growth-re-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-12-effects-muni.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-14-dic.html \
	data/modelled/lbw-25-growth-no-rain.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-02-mcmc.html \
	data/modelled/lbw-25-growth-no-rain-burned.rds \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-14-dic.html \
	docs/content/40-modelling/60-prematurity/_index.html \
	data/modelled/pre-10-full-re.rds \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-03-mcmc-continue.html \
	data/modelled/pre-10-full-re-merged.rds \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-07-merge.html \
	data/modelled/pre-10-full-re-burned.rds \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-12-effects-muni.html \
	data/modelled/pre-11-full-re.rds \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-02-mcmc.html \
	data/modelled/pre-11-full-re-burned.rds \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-14-dic.html \
	data/modelled/pre-12-full-re-p1.rds \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-02-mcmc.html \
	data/modelled/pre-12-full-re-p1-rectified.rds \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-08-mcmc-rectify.html \
	data/modelled/pre-12-full-re-p1-burned.rds \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-10-effects.html \
	data/modelled/pre-13-full-re-p2.rds \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-02-mcmc.html \
	data/modelled/pre-13-full-re-p2-rectified.rds \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-08-mcmc-rectify.html \
	data/modelled/pre-13-full-re-p2-burned.rds \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-10-effects.html \
	data/modelled/pre-15-full-no-rain.rds \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-02-mcmc.html \
	data/modelled/pre-15-full-no-rain-burned.rds \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-14-dic.html \
	docs/content/50-summarization/_index.html \
	docs/content/50-summarization/data/_index.html \
	data/summarised/paper-data-mothers-demography.csv \
	docs/content/50-summarization/data/11-mothers-demography.html \
	data/summarised/data-births-per-week.pdf \
	docs/content/50-summarization/data/12-births-per-hydroweek.html \
	data/summarised/paper-data-exposure-counts.pdf \
	data/summarised/paper-data-exposure-counts.jpg \
	data/processed/exposure-reference-points.rds \
	docs/content/50-summarization/data/22-exposure-counts.html \
	data/summarised/paper-data-bw-per-exposure.pdf \
	data/summarised/paper-data-bw-per-exposure.jpg \
	docs/content/50-summarization/data/24-lbw-bw-per-exposure.html \
	docs/content/50-summarization/models/_index.html \
	data/summarised/summary-full-models.csv \
	docs/content/50-summarization/models/11-summary-full-model.html \
	data/summarised/summary-growth-models.csv \
	docs/content/50-summarization/models/13-summary-growth-model.html \
	data/summarised/summary-prematurity-model.csv \
	docs/content/50-summarization/models/15-summary-premature-model.html \
	data/summarised/summary-prematurity-model-not-used.csv \
	docs/content/50-summarization/models/17-summary-premature-model.html \
	data/summarised/paper-mod-eff-rain-baseline-significant.pdf \
	data/summarised/paper-mod-eff-rain-baseline-significant.jpg \
	data/summarised/paper-mod-eff-rain-baseline-ci.pdf \
	docs/content/50-summarization/models/21-effects-rainfall-significant-ci-baseline.html \
	data/summarised/mod-eff-rain-baseline2-significant.pdf \
	data/summarised/mod-eff-rain-baseline2-ci.pdf \
	docs/content/50-summarization/models/22-effects-rainfall-significant-ci-baseline2.html \
	data/summarised/mod-eff-rain-nobaseline-significant.pdf \
	data/summarised/mod-eff-rain-nobaseline-ci.pdf \
	docs/content/50-summarization/models/23-effects-rainfall-significant-ci-nobaseline.html \
	data/summarised/mod-eff-rain-seasonal-deviations.pdf \
	data/summarised/mod-eff-rain-seasonal-deviations-significant.pdf \
	data/summarised/mod-eff-rain-seasonal-deviations-ci.pdf \
	data/summarised/mod-eff-rain-seasonal-deviations-weeks.pdf \
	data/summarised/mod-eff-rain-seasonal-deviations-significant-weeks.pdf \
	docs/content/50-summarization/models/25-effects-rainfall-deviations.html \
	data/summarised/mod-eff-rain-non-extremes.pdf \
	data/summarised/mod-eff-rain-non-extremes-significant.pdf \
	data/summarised/mod-eff-rain-non-extremes-ci.pdf \
	data/summarised/mod-eff-rain-non-extremes-weeks.pdf \
	data/summarised/mod-eff-rain-non-extremes-significant-weeks.pdf \
	docs/content/50-summarization/models/26-effects-rainfall-non-extremes.html \
	data/summarised/mod-eff-rain-extremes.pdf \
	data/summarised/mod-eff-rain-extremes-significant.pdf \
	data/summarised/mod-eff-rain-extremes-ci.pdf \
	data/summarised/mod-eff-rain-extremes-weeks.pdf \
	data/summarised/mod-eff-rain-extremes-significant-weeks.pdf \
	docs/content/50-summarization/models/27-effects-rainfall-extremes.html \
	data/summarised/paper-mod-eff-nonlinear-age-and-seasonality.pdf \
	docs/content/50-summarization/models/41-effects-nonlinear-age-and-seasonality.html \
	data/summarised/paper-mod-eff-nonlinear-temporal.pdf \
	docs/content/50-summarization/models/43-effects-nonlinear-temporal.html \
	data/summarised/mod-eff-nonlinear-age.pdf \
	data/summarised/mod-eff-nonlinear-seasonality.pdf \
	data/summarised/mod-eff-nonlinear-temporal.pdf \
	data/summarised/mod-eff-nonlinear-age-sigma.pdf \
	data/summarised/mod-eff-nonlinear-temporal-sigma.pdf \
	docs/content/50-summarization/models/45-effects-nonlinear.html \
	data/summarised/paper-mod-eff-municipality-covs.pdf \
	docs/content/50-summarization/models/81-effects-municipality-covariates.html \
	data/summarised/paper-mod-eff-municipalities-ci.pdf \
	data/summarised/paper-mod-eff-municipalities-ci.jpg \
	data/summarised/paper-mod-eff-municipalities-map.pdf \
	data/summarised/paper-mod-eff-municipalities-map.jpg \
	docs/content/50-summarization/models/83-effects-municipality.html \
	data/summarised/mod-eff-municipality-remoteness.pdf \
	data/summarised/mod-eff-municipality-sanitation.pdf \
	docs/content/50-summarization/models/85-effects-municipality-covariates-simple.html

target_clean = docs/content/_index.html \
	docs/content/20-processing/_index.html \
	docs/content/20-processing/01-bwdata-model.html \
	docs/content/20-processing/11-bwdata-test.html \
	docs/content/20-processing/31-ama-penalty.html \
	docs/content/30-exploration/_index.html \
	docs/content/30-exploration/40-birthweight/_index.html \
	docs/content/30-exploration/40-birthweight/02-bw-oneterm-smooths.html \
	docs/content/30-exploration/40-birthweight/03-bw-oneterm-visualize.html \
	docs/content/30-exploration/40-birthweight/04-bw-oneterm-smooths-muni.html \
	docs/content/30-exploration/40-birthweight/05-bw-oneterm-muni-visualize.html \
	docs/content/30-exploration/50-low-birthweight/_index.html \
	docs/content/30-exploration/50-low-birthweight/02-lbw-oneterm-smooths.html \
	docs/content/30-exploration/50-low-birthweight/03-lbw-oneterm-visualize.html \
	docs/content/30-exploration/50-low-birthweight/04-lbw-oneterm-smooths-muni.html \
	docs/content/30-exploration/50-low-birthweight/05-lbw-oneterm-muni-visualize.html \
	docs/content/30-exploration/60-prematurity/_index.html \
	docs/content/30-exploration/60-prematurity/02-pre-oneterm-smooths.html \
	docs/content/30-exploration/60-prematurity/03-pre-oneterm-visualize.html \
	docs/content/30-exploration/60-prematurity/04-pre-oneterm-smooths-muni.html \
	docs/content/30-exploration/60-prematurity/05-pre-oneterm-muni-visualize.html \
	docs/content/30-exploration/70-modelling/_index.html \
	docs/content/30-exploration/70-modelling/01-priors.html \
	docs/content/35-model-testing/_index.html \
	docs/content/35-model-testing/10-testing-simulated/_index.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-00-notes.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-08-mcmc-rectify.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-00-inter-10-effects.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-10-effects.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-08-mcmc-rectify.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-10-effects.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-08-mcmc-rectify.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-10-effects.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-08-mcmc-rectify.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-10-effects.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-01-fit.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-02-mcmc.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-09-burnin.html \
	docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-10-effects.html \
	docs/content/35-model-testing/20-testing-muni/_index.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-08-mcmc-rectify.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-09-burnin.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-10-effects.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-08-mcmc-rectify.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-09-burnin.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-10-effects.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-01-fit.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-02-mcmc.html \
	docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-09-burnin.html \
	docs/content/40-modelling/_index.html \
	docs/content/40-modelling/40-birthweight/_index.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-02-mcmc.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue2.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-07-merge.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-08-mcmc-rectify.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-10-full-re-t-14-dic.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-02-mcmc.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-08-mcmc-rectify.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-12-full-re-14-dic.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-02-mcmc.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-08-mcmc-rectify.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-14-dic.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-02-mcmc.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-03-continue.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-07-merge.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-08-mcmc-rectify.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-12-effects-muni.html \
	docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-14-dic.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-01-fit.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-02-mcmc.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-08-mcmc-rectify.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-09-burnin.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-10-effects.html \
	docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-14-dic.html \
	docs/content/40-modelling/50-low-birthweight/_index.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-03-mcmc-continue.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-07-merge.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-12-effects-muni.html \
	docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-14-dic.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-08-mcmc-rectify.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-08-mcmc-rectify.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-14-dic.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-12-effects-muni.html \
	docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-14-dic.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-01-fit.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-02-mcmc.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-09-burnin.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-10-effects.html \
	docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-14-dic.html \
	docs/content/40-modelling/60-prematurity/_index.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-03-mcmc-continue.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-07-merge.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-10-full-re-12-effects-muni.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-11-full-re-14-dic.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-08-mcmc-rectify.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-08-mcmc-rectify.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-01-fit.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-02-mcmc.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-09-burnin.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-10-effects.html \
	docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-14-dic.html \
	docs/content/50-summarization/_index.html \
	docs/content/50-summarization/data/_index.html \
	docs/content/50-summarization/data/11-mothers-demography.html \
	docs/content/50-summarization/data/12-births-per-hydroweek.html \
	docs/content/50-summarization/data/22-exposure-counts.html \
	docs/content/50-summarization/data/24-lbw-bw-per-exposure.html \
	docs/content/50-summarization/models/_index.html \
	docs/content/50-summarization/models/11-summary-full-model.html \
	docs/content/50-summarization/models/13-summary-growth-model.html \
	docs/content/50-summarization/models/15-summary-premature-model.html \
	docs/content/50-summarization/models/17-summary-premature-model.html \
	docs/content/50-summarization/models/21-effects-rainfall-significant-ci-baseline.html \
	docs/content/50-summarization/models/22-effects-rainfall-significant-ci-baseline2.html \
	docs/content/50-summarization/models/23-effects-rainfall-significant-ci-nobaseline.html \
	docs/content/50-summarization/models/25-effects-rainfall-deviations.html \
	docs/content/50-summarization/models/26-effects-rainfall-non-extremes.html \
	docs/content/50-summarization/models/27-effects-rainfall-extremes.html \
	docs/content/50-summarization/models/41-effects-nonlinear-age-and-seasonality.html \
	docs/content/50-summarization/models/43-effects-nonlinear-temporal.html \
	docs/content/50-summarization/models/45-effects-nonlinear.html \
	docs/content/50-summarization/models/81-effects-municipality-covariates.html \
	docs/content/50-summarization/models/83-effects-municipality.html \
	docs/content/50-summarization/models/85-effects-municipality-covariates-simple.html

all: $(target_all)

docs/content/_index.html: \
	scripts/_index.Rmd

docs/content/20-processing/_index.html: \
	scripts/20-processing/_index.Rmd

data/processed/bwdata_41_model.fst docs/content/20-processing/01-bwdata-model.html: \
	scripts/20-processing/01-bwdata-model.Rmd \
	data/processed/bwdata_31_exposure.fst

data/processed/bwdata_51_test.fst docs/content/20-processing/11-bwdata-test.html: \
	scripts/20-processing/11-bwdata-test.Rmd

data/processed/ama_10_penalty.rds docs/content/20-processing/31-ama-penalty.html: \
	scripts/20-processing/31-ama-penalty.Rmd \
	data/processed/bwdata_41_model.fst \
	data/processed/ama_05_62.gpkg

docs/content/30-exploration/_index.html: \
	scripts/30-exploration/_index.Rmd

docs/content/30-exploration/40-birthweight/_index.html: \
	scripts/30-exploration/40-birthweight/_index.Rmd

data/modelled/bw-explore-smooths.rds docs/content/30-exploration/40-birthweight/02-bw-oneterm-smooths.html: \
	scripts/30-exploration/40-birthweight/02-bw-oneterm-smooths.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/40-birthweight/03-bw-oneterm-visualize.html: \
	scripts/30-exploration/40-birthweight/03-bw-oneterm-visualize.Rmd \
	data/modelled/bw-explore-smooths.rds

data/modelled/bw-explore-smooths-muni.rds docs/content/30-exploration/40-birthweight/04-bw-oneterm-smooths-muni.html: \
	scripts/30-exploration/40-birthweight/04-bw-oneterm-smooths-muni.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/40-birthweight/05-bw-oneterm-muni-visualize.html: \
	scripts/30-exploration/40-birthweight/05-bw-oneterm-muni-visualize.Rmd \
	data/modelled/bw-explore-smooths-muni.rds

docs/content/30-exploration/50-low-birthweight/_index.html: \
	scripts/30-exploration/50-low-birthweight/_index.Rmd

data/modelled/lbw-explore-smooths.rds docs/content/30-exploration/50-low-birthweight/02-lbw-oneterm-smooths.html: \
	scripts/30-exploration/50-low-birthweight/02-lbw-oneterm-smooths.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/50-low-birthweight/03-lbw-oneterm-visualize.html: \
	scripts/30-exploration/50-low-birthweight/03-lbw-oneterm-visualize.Rmd \
	data/modelled/lbw-explore-smooths.rds

data/modelled/lbw-explore-smooths-muni.rds docs/content/30-exploration/50-low-birthweight/04-lbw-oneterm-smooths-muni.html: \
	scripts/30-exploration/50-low-birthweight/04-lbw-oneterm-smooths-muni.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/50-low-birthweight/05-lbw-oneterm-muni-visualize.html: \
	scripts/30-exploration/50-low-birthweight/05-lbw-oneterm-muni-visualize.Rmd \
	data/modelled/lbw-explore-smooths-muni.rds

docs/content/30-exploration/60-prematurity/_index.html: \
	scripts/30-exploration/60-prematurity/_index.Rmd

data/modelled/pre-explore-smooths.rds docs/content/30-exploration/60-prematurity/02-pre-oneterm-smooths.html: \
	scripts/30-exploration/60-prematurity/02-pre-oneterm-smooths.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/60-prematurity/03-pre-oneterm-visualize.html: \
	scripts/30-exploration/60-prematurity/03-pre-oneterm-visualize.Rmd \
	data/modelled/pre-explore-smooths.rds

data/modelled/pre-explore-smooths-muni.rds docs/content/30-exploration/60-prematurity/04-pre-oneterm-smooths-muni.html: \
	scripts/30-exploration/60-prematurity/04-pre-oneterm-smooths-muni.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/30-exploration/60-prematurity/05-pre-oneterm-muni-visualize.html: \
	scripts/30-exploration/60-prematurity/05-pre-oneterm-muni-visualize.Rmd \
	data/modelled/pre-explore-smooths-muni.rds

docs/content/30-exploration/70-modelling/_index.html: \
	scripts/30-exploration/70-modelling/_index.Rmd

docs/content/30-exploration/70-modelling/01-priors.html: \
	scripts/30-exploration/70-modelling/01-priors.Rmd

docs/content/35-model-testing/_index.html: \
	scripts/35-model-testing/_index.Rmd

docs/content/35-model-testing/10-testing-simulated/_index.html: \
	scripts/35-model-testing/10-testing-simulated/_index.Rmd

docs/content/35-model-testing/10-testing-simulated/bw-00-00-notes.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-00-notes.Rmd

data/modelled/bw-00-inter.rds docs/content/35-model-testing/10-testing-simulated/bw-00-inter-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-inter-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-00-inter-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-inter-02-mcmc.Rmd \
	data/modelled/bw-00-inter.rds

data/modelled/bw-00-inter-rectified.rds docs/content/35-model-testing/10-testing-simulated/bw-00-inter-08-mcmc-rectify.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-inter-08-mcmc-rectify.Rmd \
	data/modelled/bw-00-inter.rds

data/modelled/bw-00-inter-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-00-inter-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-inter-09-burnin.Rmd \
	data/modelled/bw-00-inter-rectified.rds

docs/content/35-model-testing/10-testing-simulated/bw-00-inter-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-00-inter-10-effects.Rmd \
	data/modelled/bw-00-inter-burned.rds

data/modelled/bw-02-nointer.rds docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-02-nointer-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-02-nointer-02-mcmc.Rmd \
	data/modelled/bw-02-nointer.rds

data/modelled/bw-02-nointer-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-02-nointer-09-burnin.Rmd \
	data/modelled/bw-02-nointer.rds

docs/content/35-model-testing/10-testing-simulated/bw-02-nointer-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-02-nointer-10-effects.Rmd \
	data/modelled/bw-02-nointer-burned.rds

data/modelled/bw-04-inter-bin.rds docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-04-inter-bin-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-04-inter-bin-02-mcmc.Rmd \
	data/modelled/bw-04-inter-bin.rds

data/modelled/bw-04-inter-bin-rectified.rds docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-08-mcmc-rectify.html: \
	scripts/35-model-testing/10-testing-simulated/bw-04-inter-bin-08-mcmc-rectify.Rmd \
	data/modelled/bw-04-inter-bin.rds

data/modelled/bw-04-inter-bin-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-04-inter-bin-09-burnin.Rmd \
	data/modelled/bw-04-inter-bin-rectified.rds

docs/content/35-model-testing/10-testing-simulated/bw-04-inter-bin-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-04-inter-bin-10-effects.Rmd \
	data/modelled/bw-04-inter-bin-burned.rds

data/modelled/bw-06-discrete.rds docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-06-discrete-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-06-discrete-02-mcmc.Rmd \
	data/modelled/bw-06-discrete.rds

data/modelled/bw-06-discrete-rectified.rds docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-08-mcmc-rectify.html: \
	scripts/35-model-testing/10-testing-simulated/bw-06-discrete-08-mcmc-rectify.Rmd \
	data/modelled/bw-06-discrete.rds

data/modelled/bw-06-discrete-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-06-discrete-09-burnin.Rmd \
	data/modelled/bw-06-discrete-rectified.rds

docs/content/35-model-testing/10-testing-simulated/bw-06-discrete-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-06-discrete-10-effects.Rmd \
	data/modelled/bw-06-discrete-burned.rds

data/modelled/bw-08-discrete-bin.rds docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-08-discrete-bin-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-08-discrete-bin-02-mcmc.Rmd \
	data/modelled/bw-08-discrete-bin.rds

data/modelled/bw-08-discrete-bin-rectified.rds docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-08-mcmc-rectify.html: \
	scripts/35-model-testing/10-testing-simulated/bw-08-discrete-bin-08-mcmc-rectify.Rmd \
	data/modelled/bw-08-discrete-bin.rds

data/modelled/bw-08-discrete-bin-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-08-discrete-bin-09-burnin.Rmd \
	data/modelled/bw-08-discrete-bin-rectified.rds

docs/content/35-model-testing/10-testing-simulated/bw-08-discrete-bin-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-08-discrete-bin-10-effects.Rmd \
	data/modelled/bw-08-discrete-bin-burned.rds

data/modelled/bw-09-discrete-bin-age.rds docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-01-fit.html: \
	scripts/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-01-fit.Rmd \
	data/processed/bwdata_51_test.fst

docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-02-mcmc.html: \
	scripts/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-02-mcmc.Rmd \
	data/modelled/bw-09-discrete-bin-age.rds

data/modelled/bw-09-discrete-bin-age-burned.rds docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-09-burnin.html: \
	scripts/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-09-burnin.Rmd \
	data/modelled/bw-09-discrete-bin-age.rds

docs/content/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-10-effects.html: \
	scripts/35-model-testing/10-testing-simulated/bw-09-discrete-bin-age-10-effects.Rmd \
	data/modelled/bw-09-discrete-bin-age-burned.rds

docs/content/35-model-testing/20-testing-muni/_index.html: \
	scripts/35-model-testing/20-testing-muni/_index.Rmd

data/modelled/bw-muni-08-lm-re-rectified.rds docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-08-mcmc-rectify.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-08-lm-re-08-mcmc-rectify.Rmd \
	data/modelled/bw-muni-08-lm-re.rds

data/modelled/bw-muni-08-lm-re-burned.rds docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-09-burnin.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-08-lm-re-09-burnin.Rmd \
	data/modelled/bw-muni-08-lm-re-rectified.rds

docs/content/35-model-testing/20-testing-muni/bw-muni-08-lm-re-10-effects.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-08-lm-re-10-effects.Rmd \
	data/modelled/bw-muni-08-lm-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-muni-10-slm-re.rds docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-10-slm-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-10-slm-re-02-mcmc.Rmd \
	data/modelled/bw-muni-10-slm-re.rds

data/modelled/bw-muni-10-slm-re-rectified.rds docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-08-mcmc-rectify.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-10-slm-re-08-mcmc-rectify.Rmd \
	data/modelled/bw-muni-10-slm-re.rds

data/modelled/bw-muni-10-slm-re-burned.rds docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-09-burnin.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-10-slm-re-09-burnin.Rmd \
	data/modelled/bw-muni-10-slm-re-rectified.rds

docs/content/35-model-testing/20-testing-muni/bw-muni-10-slm-re-10-effects.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-10-slm-re-10-effects.Rmd \
	data/modelled/bw-muni-10-slm-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-muni-12-remote-re.rds docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-12-remote-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-12-remote-re-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-12-remote-re-02-mcmc.Rmd \
	data/modelled/bw-muni-12-remote-re.rds

data/modelled/bw-muni-13-remote-re-bin.rds docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-13-remote-re-bin-02-mcmc.Rmd \
	data/modelled/bw-muni-13-remote-re-bin.rds

data/modelled/bw-muni-14-water-re.rds docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-14-water-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-14-water-re-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-14-water-re-02-mcmc.Rmd \
	data/modelled/bw-muni-14-water-re.rds

data/modelled/bw-muni-16-sp-re.rds docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-16-sp-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-16-sp-re-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-16-sp-re-02-mcmc.Rmd \
	data/modelled/bw-muni-16-sp-re.rds

data/modelled/bw-muni-17-sp-re-bin.rds docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-17-sp-re-bin-02-mcmc.Rmd \
	data/modelled/bw-muni-17-sp-re-bin.rds

data/modelled/bw-muni-18-mrf-re.rds docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst \
	data/processed/ama_10_penalty.rds

docs/content/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-18-mrf-re-02-mcmc.Rmd \
	data/modelled/bw-muni-18-mrf-re.rds

data/modelled/bw-muni-19-mrf-re-bin.rds docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-01-fit.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-01-fit.Rmd \
	data/processed/bwdata_41_model.fst \
	data/processed/ama_10_penalty.rds

docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-02-mcmc.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-02-mcmc.Rmd \
	data/modelled/bw-muni-19-mrf-re-bin.rds

data/modelled/bw-muni-19-mrf-re-bin-burned.rds docs/content/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-09-burnin.html: \
	scripts/35-model-testing/20-testing-muni/bw-muni-19-mrf-re-bin-09-burnin.Rmd \
	data/modelled/bw-muni-19-mrf-re-bin.rds

docs/content/40-modelling/_index.html: \
	scripts/40-modelling/_index.Rmd

docs/content/40-modelling/40-birthweight/_index.html: \
	scripts/40-modelling/40-birthweight/_index.Rmd

data/modelled/bw-10-full-re-t.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-01-fit.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-10-full-re-t-02-mcmc.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-02-mcmc.Rmd \
	data/modelled/bw-10-full-re-t.rds

data/modelled/bw-10-full-re-t-continue.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-03-continue.Rmd \
	src/43-mcmc-continue-light.R \
	src/45-mcmc-reshape.R \
	src/46-mcmc-last-sample.R \
	data/modelled/bw-10-full-re-t.rds

data/modelled/bw-10-full-re-t-continue2.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-03-continue2.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-03-continue2.Rmd \
	data/modelled/bw-10-full-re-t.rds

data/modelled/bw-10-full-re-t-merged.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-07-merge.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-07-merge.Rmd \
	data/modelled/bw-10-full-re-t.rds \
	data/modelled/bw-10-full-re-t-continue.rds \
	data/modelled/bw-10-full-re-t-continue2.rds

data/modelled/bw-10-full-re-t-rectified.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-08-mcmc-rectify.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-08-mcmc-rectify.Rmd \
	data/modelled/bw-10-full-re-t-merged.rds

data/modelled/bw-10-full-re-t-burned.rds docs/content/40-modelling/40-birthweight/bw-10-full-re-t-09-burnin.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-09-burnin.Rmd \
	data/modelled/bw-10-full-re-t-rectified.rds

docs/content/40-modelling/40-birthweight/bw-10-full-re-t-10-effects.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-10-effects.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-10-full-re-t-12-effects-muni.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-12-effects-muni.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-10-full-re-t-14-dic.html: \
	scripts/40-modelling/40-birthweight/bw-10-full-re-t-14-dic.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-12-full-re.rds docs/content/40-modelling/40-birthweight/bw-12-full-re-01-fit.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-12-full-re-02-mcmc.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-02-mcmc.Rmd \
	data/modelled/bw-12-full-re.rds

data/modelled/bw-12-full-re-rectified.rds docs/content/40-modelling/40-birthweight/bw-12-full-re-08-mcmc-rectify.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-08-mcmc-rectify.Rmd \
	data/modelled/bw-12-full-re.rds

data/modelled/bw-12-full-re-burned.rds docs/content/40-modelling/40-birthweight/bw-12-full-re-09-burnin.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-09-burnin.Rmd \
	data/modelled/bw-12-full-re-rectified.rds

docs/content/40-modelling/40-birthweight/bw-12-full-re-10-effects.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-10-effects.Rmd \
	data/modelled/bw-12-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-12-full-re-12-effects-muni.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-12-effects-muni.Rmd \
	data/modelled/bw-12-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-12-full-re-14-dic.html: \
	scripts/40-modelling/40-birthweight/bw-12-full-re-14-dic.Rmd \
	data/modelled/bw-12-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-15-full-no-rain-t.rds docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-01-fit.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-02-mcmc.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-02-mcmc.Rmd \
	data/modelled/bw-15-full-no-rain-t.rds

data/modelled/bw-15-full-no-rain-t-rectified.rds docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-08-mcmc-rectify.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-08-mcmc-rectify.Rmd \
	data/modelled/bw-15-full-no-rain-t.rds

data/modelled/bw-15-full-no-rain-t-burned.rds docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-09-burnin.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-09-burnin.Rmd \
	data/modelled/bw-15-full-no-rain-t-rectified.rds

docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-10-effects.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-10-effects.Rmd \
	data/modelled/bw-15-full-no-rain-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-15-full-no-rain-t-14-dic.html: \
	scripts/40-modelling/40-birthweight/bw-15-full-no-rain-t-14-dic.Rmd \
	data/modelled/bw-15-full-no-rain-t-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-20-growth-re-t.rds docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-01-fit.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-02-mcmc.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-02-mcmc.Rmd \
	data/modelled/bw-20-growth-re-t.rds

data/modelled/bw-20-growth-re-t-continue.rds docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-03-continue.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-03-continue.Rmd \
	src/43-mcmc-continue-light.R \
	src/45-mcmc-reshape.R \
	src/46-mcmc-last-sample.R \
	data/modelled/bw-20-growth-re-t.rds

data/modelled/bw-20-growth-re-t-merged.rds docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-07-merge.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-07-merge.Rmd \
	data/modelled/bw-20-growth-re-t.rds \
	data/modelled/bw-20-growth-re-t-continue.rds \
	data/modelled/bw-20-growth-re-t-continue2.rds

data/modelled/bw-20-growth-re-t-rectified.rds docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-08-mcmc-rectify.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-08-mcmc-rectify.Rmd \
	data/modelled/bw-20-growth-re-t-merged.rds

data/modelled/bw-20-growth-re-t-burned.rds docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-09-burnin.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-09-burnin.Rmd \
	data/modelled/bw-20-growth-re-t-rectified.rds

docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-10-effects.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-10-effects.Rmd \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-12-effects-muni.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-12-effects-muni.Rmd \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-20-growth-re-t-14-dic.html: \
	scripts/40-modelling/40-birthweight/bw-20-growth-re-t-14-dic.Rmd \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/bw-25-growth-no-rain-t.rds docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-01-fit.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-02-mcmc.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-02-mcmc.Rmd \
	data/modelled/bw-25-growth-no-rain-t.rds

data/modelled/bw-25-growth-no-rain-t-rectified.rds docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-08-mcmc-rectify.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-08-mcmc-rectify.Rmd \
	data/modelled/bw-25-growth-no-rain-t.rds

data/modelled/bw-25-growth-no-rain-t-burned.rds docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-09-burnin.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-09-burnin.Rmd \
	data/modelled/bw-25-growth-no-rain-t-rectified.rds

docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-10-effects.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-10-effects.Rmd \
	data/modelled/bw-25-growth-no-rain-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/40-birthweight/bw-25-growth-no-rain-t-14-dic.html: \
	scripts/40-modelling/40-birthweight/bw-25-growth-no-rain-t-14-dic.Rmd \
	data/modelled/bw-25-growth-no-rain-t-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/_index.html: \
	scripts/40-modelling/50-low-birthweight/_index.Rmd

data/modelled/lbw-10-full-re.rds docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-02-mcmc.Rmd \
	data/modelled/lbw-10-full-re.rds

docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-03-mcmc-continue.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-03-mcmc-continue.Rmd \
	src/43-mcmc-continue-light.R \
	src/45-mcmc-reshape.R \
	src/46-mcmc-last-sample.R \
	data/modelled/lbw-10-full-re.rds

data/modelled/lbw-10-full-re-merged.rds docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-07-merge.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-07-merge.Rmd \
	data/modelled/lbw-10-full-re.rds \
	data/modelled/lbw-10-full-re-continue.rds \
	src/47-mcmc-merge-samples.R

data/modelled/lbw-10-full-re-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-09-burnin.Rmd \
	data/modelled/lbw-10-full-re-merged.rds

docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-10-effects.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-10-effects.Rmd \
	data/modelled/lbw-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-12-effects-muni.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-12-effects-muni.Rmd \
	data/modelled/lbw-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-10-full-re-14-dic.html: \
	scripts/40-modelling/50-low-birthweight/lbw-10-full-re-14-dic.Rmd \
	data/modelled/lbw-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/lbw-11-full-re-p1.rds docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-11-full-re-p1-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-11-full-re-p1-02-mcmc.Rmd \
	data/modelled/lbw-11-full-re-p1.rds

data/modelled/lbw-11-full-re-p1-rectified.rds docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-08-mcmc-rectify.html: \
	scripts/40-modelling/50-low-birthweight/lbw-11-full-re-p1-08-mcmc-rectify.Rmd \
	data/modelled/lbw-11-full-re-p1.rds

data/modelled/lbw-11-full-re-p1-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-11-full-re-p1-09-burnin.Rmd \
	data/modelled/lbw-11-full-re-p1-rectified.rds

docs/content/40-modelling/50-low-birthweight/lbw-11-full-re-p1-10-effects.html: \
	scripts/40-modelling/50-low-birthweight/lbw-11-full-re-p1-10-effects.Rmd \
	data/modelled/lbw-11-full-re-p1-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/lbw-12-full-re-p2.rds docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-12-full-re-p2-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-12-full-re-p2-02-mcmc.Rmd \
	data/modelled/lbw-12-full-re-p2.rds

data/modelled/lbw-12-full-re-p2-rectified.rds docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-08-mcmc-rectify.html: \
	scripts/40-modelling/50-low-birthweight/lbw-12-full-re-p2-08-mcmc-rectify.Rmd \
	data/modelled/lbw-12-full-re-p2.rds

data/modelled/lbw-12-full-re-p2-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-12-full-re-p2-09-burnin.Rmd \
	data/modelled/lbw-12-full-re-p2-rectified.rds

docs/content/40-modelling/50-low-birthweight/lbw-12-full-re-p2-10-effects.html: \
	scripts/40-modelling/50-low-birthweight/lbw-12-full-re-p2-10-effects.Rmd \
	data/modelled/lbw-12-full-re-p2-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/lbw-15-full-no-rain.rds docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-15-full-no-rain-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-15-full-no-rain-02-mcmc.Rmd \
	data/modelled/lbw-15-full-no-rain.rds

data/modelled/lbw-15-full-no-rain-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-15-full-no-rain-09-burnin.Rmd \
	data/modelled/lbw-15-full-no-rain.rds

docs/content/40-modelling/50-low-birthweight/lbw-15-full-no-rain-14-dic.html: \
	scripts/40-modelling/50-low-birthweight/lbw-15-full-no-rain-14-dic.Rmd \
	data/modelled/lbw-15-full-no-rain-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/lbw-20-growth-re.rds docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-02-mcmc.Rmd \
	data/modelled/lbw-20-growth-re.rds

data/modelled/lbw-20-growth-re-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-09-burnin.Rmd \
	data/modelled/lbw-20-growth-re.rds

docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-10-effects.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-10-effects.Rmd \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-12-effects-muni.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-12-effects-muni.Rmd \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-20-growth-re-14-dic.html: \
	scripts/40-modelling/50-low-birthweight/lbw-20-growth-re-14-dic.Rmd \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/lbw-25-growth-no-rain.rds docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-01-fit.html: \
	scripts/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-02-mcmc.html: \
	scripts/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-02-mcmc.Rmd \
	data/modelled/lbw-25-growth-no-rain.rds

data/modelled/lbw-25-growth-no-rain-burned.rds docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-09-burnin.html: \
	scripts/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-09-burnin.Rmd \
	data/modelled/lbw-25-growth-no-rain.rds

docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-10-effects.html: \
	scripts/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-10-effects.Rmd \
	data/modelled/lbw-25-growth-no-rain-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-14-dic.html: \
	scripts/40-modelling/50-low-birthweight/lbw-25-growth-no-rain-14-dic.Rmd \
	data/modelled/lbw-25-growth-no-rain-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/_index.html: \
	scripts/40-modelling/60-prematurity/_index.Rmd

data/modelled/pre-10-full-re.rds docs/content/40-modelling/60-prematurity/pre-10-full-re-01-fit.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-10-full-re-02-mcmc.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-02-mcmc.Rmd \
	data/modelled/pre-10-full-re.rds

docs/content/40-modelling/60-prematurity/pre-10-full-re-03-mcmc-continue.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-03-mcmc-continue.Rmd \
	src/43-mcmc-continue-light.R \
	src/45-mcmc-reshape.R \
	src/46-mcmc-last-sample.R \
	data/modelled/pre-10-full-re.rds

data/modelled/pre-10-full-re-merged.rds docs/content/40-modelling/60-prematurity/pre-10-full-re-07-merge.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-07-merge.Rmd \
	data/modelled/pre-10-full-re.rds \
	data/modelled/pre-10-full-re-continue.rds \
	src/47-mcmc-merge-samples.R

data/modelled/pre-10-full-re-burned.rds docs/content/40-modelling/60-prematurity/pre-10-full-re-09-burnin.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-09-burnin.Rmd \
	data/modelled/pre-10-full-re-merged.rds

docs/content/40-modelling/60-prematurity/pre-10-full-re-10-effects.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-10-effects.Rmd \
	data/modelled/pre-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-10-full-re-12-effects-muni.html: \
	scripts/40-modelling/60-prematurity/pre-10-full-re-12-effects-muni.Rmd \
	data/modelled/pre-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/pre-11-full-re.rds docs/content/40-modelling/60-prematurity/pre-11-full-re-01-fit.html: \
	scripts/40-modelling/60-prematurity/pre-11-full-re-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-11-full-re-02-mcmc.html: \
	scripts/40-modelling/60-prematurity/pre-11-full-re-02-mcmc.Rmd \
	data/modelled/pre-11-full-re.rds

data/modelled/pre-11-full-re-burned.rds docs/content/40-modelling/60-prematurity/pre-11-full-re-09-burnin.html: \
	scripts/40-modelling/60-prematurity/pre-11-full-re-09-burnin.Rmd \
	data/modelled/pre-11-full-re.rds

docs/content/40-modelling/60-prematurity/pre-11-full-re-10-effects.html: \
	scripts/40-modelling/60-prematurity/pre-11-full-re-10-effects.Rmd \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-11-full-re-14-dic.html: \
	scripts/40-modelling/60-prematurity/pre-11-full-re-14-dic.Rmd \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/pre-12-full-re-p1.rds docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-01-fit.html: \
	scripts/40-modelling/60-prematurity/pre-12-full-re-p1-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-02-mcmc.html: \
	scripts/40-modelling/60-prematurity/pre-12-full-re-p1-02-mcmc.Rmd \
	data/modelled/pre-12-full-re-p1.rds

data/modelled/pre-12-full-re-p1-rectified.rds docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-08-mcmc-rectify.html: \
	scripts/40-modelling/60-prematurity/pre-12-full-re-p1-08-mcmc-rectify.Rmd \
	data/modelled/pre-12-full-re-p1.rds

data/modelled/pre-12-full-re-p1-burned.rds docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-09-burnin.html: \
	scripts/40-modelling/60-prematurity/pre-12-full-re-p1-09-burnin.Rmd \
	data/modelled/pre-12-full-re-p1-rectified.rds

docs/content/40-modelling/60-prematurity/pre-12-full-re-p1-10-effects.html: \
	scripts/40-modelling/60-prematurity/pre-12-full-re-p1-10-effects.Rmd \
	data/modelled/pre-12-full-re-p1-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/pre-13-full-re-p2.rds docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-01-fit.html: \
	scripts/40-modelling/60-prematurity/pre-13-full-re-p2-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-02-mcmc.html: \
	scripts/40-modelling/60-prematurity/pre-13-full-re-p2-02-mcmc.Rmd \
	data/modelled/pre-13-full-re-p2.rds

data/modelled/pre-13-full-re-p2-rectified.rds docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-08-mcmc-rectify.html: \
	scripts/40-modelling/60-prematurity/pre-13-full-re-p2-08-mcmc-rectify.Rmd \
	data/modelled/pre-13-full-re-p2.rds

data/modelled/pre-13-full-re-p2-burned.rds docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-09-burnin.html: \
	scripts/40-modelling/60-prematurity/pre-13-full-re-p2-09-burnin.Rmd \
	data/modelled/pre-13-full-re-p2-rectified.rds

docs/content/40-modelling/60-prematurity/pre-13-full-re-p2-10-effects.html: \
	scripts/40-modelling/60-prematurity/pre-13-full-re-p2-10-effects.Rmd \
	data/modelled/pre-13-full-re-p2-burned.rds \
	data/processed/bwdata_41_model.fst

data/modelled/pre-15-full-no-rain.rds docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-01-fit.html: \
	scripts/40-modelling/60-prematurity/pre-15-full-no-rain-01-fit.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-02-mcmc.html: \
	scripts/40-modelling/60-prematurity/pre-15-full-no-rain-02-mcmc.Rmd \
	data/modelled/pre-15-full-no-rain.rds

data/modelled/pre-15-full-no-rain-burned.rds docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-09-burnin.html: \
	scripts/40-modelling/60-prematurity/pre-15-full-no-rain-09-burnin.Rmd \
	data/modelled/pre-15-full-no-rain.rds

docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-10-effects.html: \
	scripts/40-modelling/60-prematurity/pre-15-full-no-rain-10-effects.Rmd \
	data/modelled/pre-15-full-no-rain-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/40-modelling/60-prematurity/pre-15-full-no-rain-14-dic.html: \
	scripts/40-modelling/60-prematurity/pre-15-full-no-rain-14-dic.Rmd \
	data/modelled/pre-15-full-no-rain-burned.rds \
	data/processed/bwdata_41_model.fst

docs/content/50-summarization/_index.html: \
	scripts/50-summarization/_index.Rmd

docs/content/50-summarization/data/_index.html: \
	scripts/50-summarization/data/_index.Rmd

data/summarised/paper-data-mothers-demography.csv docs/content/50-summarization/data/11-mothers-demography.html: \
	scripts/50-summarization/data/11-mothers-demography.Rmd \
	data/processed/bwdata_41_model.fst

data/summarised/data-births-per-week.pdf docs/content/50-summarization/data/12-births-per-hydroweek.html: \
	scripts/50-summarization/data/12-births-per-hydroweek.Rmd \
	data/processed/bwdata_41_model.fst

data/summarised/paper-data-exposure-counts.pdf data/summarised/paper-data-exposure-counts.jpg data/processed/exposure-reference-points.rds docs/content/50-summarization/data/22-exposure-counts.html: \
	scripts/50-summarization/data/22-exposure-counts.Rmd \
	data/processed/bwdata_41_model.fst

data/summarised/paper-data-bw-per-exposure.pdf data/summarised/paper-data-bw-per-exposure.jpg docs/content/50-summarization/data/24-lbw-bw-per-exposure.html: \
	scripts/50-summarization/data/24-lbw-bw-per-exposure.Rmd \
	data/processed/bwdata_41_model.fst

docs/content/50-summarization/models/_index.html: \
	scripts/50-summarization/models/_index.Rmd

data/summarised/summary-full-models.csv docs/content/50-summarization/models/11-summary-full-model.html: \
	scripts/50-summarization/models/11-summary-full-model.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/summarised/summary-growth-models.csv docs/content/50-summarization/models/13-summary-growth-model.html: \
	scripts/50-summarization/models/13-summary-growth-model.Rmd \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/summarised/summary-prematurity-model.csv docs/content/50-summarization/models/15-summary-premature-model.html: \
	scripts/50-summarization/models/15-summary-premature-model.Rmd \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/summarised/summary-prematurity-model-not-used.csv docs/content/50-summarization/models/17-summary-premature-model.html: \
	scripts/50-summarization/models/17-summary-premature-model.Rmd \
	data/modelled/pre-10-full-re-burned.rds \
	data/processed/bwdata_41_model.fst

data/summarised/paper-mod-eff-rain-baseline-significant.pdf data/summarised/paper-mod-eff-rain-baseline-significant.jpg data/summarised/paper-mod-eff-rain-baseline-ci.pdf docs/content/50-summarization/models/21-effects-rainfall-significant-ci-baseline.html: \
	scripts/50-summarization/models/21-effects-rainfall-significant-ci-baseline.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/mod-eff-rain-baseline2-significant.pdf data/summarised/mod-eff-rain-baseline2-ci.pdf docs/content/50-summarization/models/22-effects-rainfall-significant-ci-baseline2.html: \
	scripts/50-summarization/models/22-effects-rainfall-significant-ci-baseline2.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/mod-eff-rain-nobaseline-significant.pdf data/summarised/mod-eff-rain-nobaseline-ci.pdf docs/content/50-summarization/models/23-effects-rainfall-significant-ci-nobaseline.html: \
	scripts/50-summarization/models/23-effects-rainfall-significant-ci-nobaseline.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/mod-eff-rain-seasonal-deviations.pdf data/summarised/mod-eff-rain-seasonal-deviations-significant.pdf data/summarised/mod-eff-rain-seasonal-deviations-ci.pdf data/summarised/mod-eff-rain-seasonal-deviations-weeks.pdf data/summarised/mod-eff-rain-seasonal-deviations-significant-weeks.pdf docs/content/50-summarization/models/25-effects-rainfall-deviations.html: \
	scripts/50-summarization/models/25-effects-rainfall-deviations.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/mod-eff-rain-non-extremes.pdf data/summarised/mod-eff-rain-non-extremes-significant.pdf data/summarised/mod-eff-rain-non-extremes-ci.pdf data/summarised/mod-eff-rain-non-extremes-weeks.pdf data/summarised/mod-eff-rain-non-extremes-significant-weeks.pdf docs/content/50-summarization/models/26-effects-rainfall-non-extremes.html: \
	scripts/50-summarization/models/26-effects-rainfall-non-extremes.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/mod-eff-rain-extremes.pdf data/summarised/mod-eff-rain-extremes-significant.pdf data/summarised/mod-eff-rain-extremes-ci.pdf data/summarised/mod-eff-rain-extremes-weeks.pdf data/summarised/mod-eff-rain-extremes-significant-weeks.pdf docs/content/50-summarization/models/27-effects-rainfall-extremes.html: \
	scripts/50-summarization/models/27-effects-rainfall-extremes.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	data/processed/exposure-reference-points.rds \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/53-vis-bw-lbw.R

data/summarised/paper-mod-eff-nonlinear-age-and-seasonality.pdf docs/content/50-summarization/models/41-effects-nonlinear-age-and-seasonality.html: \
	scripts/50-summarization/models/41-effects-nonlinear-age-and-seasonality.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/56-bamlss-vis-gg.R

data/summarised/paper-mod-eff-nonlinear-temporal.pdf docs/content/50-summarization/models/43-effects-nonlinear-temporal.html: \
	scripts/50-summarization/models/43-effects-nonlinear-temporal.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/56-bamlss-vis-gg.R

data/summarised/mod-eff-nonlinear-age.pdf data/summarised/mod-eff-nonlinear-seasonality.pdf data/summarised/mod-eff-nonlinear-temporal.pdf data/summarised/mod-eff-nonlinear-age-sigma.pdf data/summarised/mod-eff-nonlinear-temporal-sigma.pdf docs/content/50-summarization/models/45-effects-nonlinear.html: \
	scripts/50-summarization/models/45-effects-nonlinear.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R

data/summarised/paper-mod-eff-municipality-covs.pdf docs/content/50-summarization/models/81-effects-municipality-covariates.html: \
	scripts/50-summarization/models/81-effects-municipality-covariates.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/56-bamlss-vis-gg.R

data/summarised/paper-mod-eff-municipalities-ci.pdf data/summarised/paper-mod-eff-municipalities-ci.jpg data/summarised/paper-mod-eff-municipalities-map.pdf data/summarised/paper-mod-eff-municipalities-map.jpg docs/content/50-summarization/models/83-effects-municipality.html: \
	scripts/50-summarization/models/83-effects-municipality.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R \
	src/56-bamlss-vis-gg.R

data/summarised/mod-eff-municipality-remoteness.pdf data/summarised/mod-eff-municipality-sanitation.pdf docs/content/50-summarization/models/85-effects-municipality-covariates-simple.html: \
	scripts/50-summarization/models/85-effects-municipality-covariates-simple.Rmd \
	data/modelled/bw-10-full-re-t-burned.rds \
	data/modelled/bw-20-growth-re-t-burned.rds \
	data/modelled/lbw-10-full-re-burned.rds \
	data/modelled/lbw-20-growth-re-burned.rds \
	data/modelled/pre-11-full-re-burned.rds \
	data/processed/bwdata_41_model.fst \
	src/51-bamlss.R \
	src/57-bamlss-vis-paper.R

$(target_all):
	@Rscript -e 'blogdown:::build_rmds("$(<D)/$(<F)", "docs", "scripts")'

clean:
	rm -f $(target_clean)


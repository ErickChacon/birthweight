---
title: "Linear model with random effects: fitting"
prerequisites:
    - data/processed/bwdata_41_model.fst
targets:
    - data/modelled/bw-muni-08-lm-re.rds
makefile: false
---

Birthweight model including covariates at municipality level with linear effects.

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 6, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.
```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)

path_proj <- day2day::git_path()
path_data <- file.path(path_proj, "data")
path_processed <- file.path(path_data, "processed")
path_modelled <- file.path(path_data, "modelled")

path_modelled_data <- file.path(path_modelled, "bw-muni-08-lm-re.rds")
path_modelled_sink <- gsub("\\.rds$", "\\.txt", path_modelled_data)
path_modelled_form <- gsub("(\\.rds)$", "-form\\1", path_modelled_data)

bwdata_model <- fst::read_fst(file.path(path_processed, "bwdata_41_model.fst"))
```

## Define formula for our model

Now we define the same models as in the previous study.
```{r}
form_sigma <- sigma ~ 1

form_mu <- born_weight ~ remoteness + prop_tap_toilet + s(res_muni, bs = "re")

form <- list(form_mu, form_sigma)
```

## Run the model of interest and save results

```{r}
{
    sink(path_modelled_sink)
    bamlss_model <- bamlss(
        form, data = bwdata_model,
        n.iter = 1000, burnin = 0, cores = 4, combine = FALSE, light = TRUE
    )
    sink()
}
readLines(path_modelled_sink)
```

```{r}
system.time(saveRDS(bamlss_model, file = path_modelled_data))
saveRDS(form, file = path_modelled_form)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

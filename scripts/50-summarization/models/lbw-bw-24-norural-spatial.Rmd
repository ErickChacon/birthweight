---
title: "Visualization of the spatial effects"
author: "Erick A. Chacón-Montalván"
date: "`r format(Sys.time(), '%d %B %Y, %H:%M:%S', usetz = TRUE)`"
output: rmarkdown::html_vignette
fontsize: 12pt
makefile: false
---

```{r setup, include = FALSE}
knitr::opts_chunk$set(echo = TRUE, fig.align = "center")
knitr::opts_chunk$set(fig.width = 10, fig.height = 4)
knitr::opts_chunk$set(comment = "#>")
options(width = 100)
```

## Load packages, read data and source custom scripts

Paths are defined relative to the git repository location.

```{r}
rm(list = ls())
library(bamlss)
library(gamlss.dist)
library(sf)
library(ggplot2)
library(patchwork)

path_proj <- day2day::git_path()
pathfi_proj <- dirname(dirname(path_proj))
pathfi_data <- file.path(pathfi_proj, "30-data")
pathfi_processed <- file.path(pathfi_data, "30-processed")
path_data <- file.path(path_proj, "30-data")
path_processed <- file.path(path_data, "30-processed")
path_output <- file.path(path_proj, "60-outputs")
path_out_model <- file.path(path_output, "40-modelling")
path_out_model_fit <- file.path(path_out_model, "10-fitting")
path_out_sum_model <- file.path(path_output, "50-summarization", "models")
path_out_aux <- file.path(path_out_sum_model, "lbw-bw-20-norural")

dir.create(path_out_aux, showWarnings = FALSE)

# load Amazonas data
(load(file.path(pathfi_processed, "ama_05_62.RData")))
# load models
model_bw_name <- load(file = file.path(path_out_model_fit, "bw-bamlss-16-norural.RData"))
model_lbw_name <- load(file = file.path(path_out_model_fit, "lbw-bam-16-norural.RData"))
model_bw <- get(model_bw_name)
model_lbw <- get(model_lbw_name)
if (model_bw_name != "model_bw") rm(list = c(model_bw_name))
if (model_lbw_name != "model_lbw") rm(list = c(model_lbw_name))

path_src <- file.path(path_proj, "40-src")
source(file.path(path_src, "51-bamlss.R"))
```

## Spatial effects for mean birth weight and low birth weight

```{r}
ama_study <- dplyr::filter(st_as_sf(ama), inc_study == 1)
term_sp <- "s(longitud,latitud)"
grid <- 100

# mu sp effects
mu_sp_eff_wide <- predict_grid(
    model_bw, "mu", term_sp, grid, intercept = FALSE, sf = ama_study,
    FUN = function (x) c95_expression(x, prefix = paste0("(", letters[4:6], ") ")),
    raster_attr = TRUE
    )
mu_sp_eff <- pred_grid_to_long(mu_sp_eff_wide)
gg_mu_sp <- ggplot_3d_bamlss(mu_sp_eff, binwidth = 6, skip = 1) +
    coord_sf(crs = sf::st_crs(ama_study)) +
    labs(x = NULL, y = NULL) +
    scale_x_continuous() +
    scale_y_continuous()

# pi sp effects
pi_sp_eff_wide <- predict_grid(
    model_lbw, "pi", term_sp, grid, intercept = FALSE, sf = ama_study,
    FUN = function (x) c95_expression(exp(x), "odds")
    )
pi_sp_eff <- pred_grid_to_long(pi_sp_eff_wide)
gg_pi_sp <- ggplot_3d_bamlss(pi_sp_eff, binwidth = 0.2,  skip = 1, trans = "log", rev = TRUE) +
    coord_sf(crs = sf::st_crs(ama_study)) +
    labs(x = NULL, y = NULL) +
    scale_x_continuous() +
    scale_y_continuous()

# pi amd mu: spatial effects
gg_sp <- (gg_pi_sp / gg_mu_sp) &
    theme(plot.margin = margin(0, 0, 0, 0, 'cm'))
ggsave(file.path(path_out_aux, 'eff_spatial.pdf'), gg_sp, width = 20, height = 10.5, units = "cm")
```

## Spatial prediction data to be saved as tif

```{r}
# convert prediction to raster
sp_eff_wide <- dplyr::left_join(pi_sp_eff_wide, mu_sp_eff_wide, c("longitud", "latitud", "id"))
names(sp_eff_wide)[4:9] <- c("odds_lower", "odds_mean", "odds_upper",
                             "mean_lower", "mean_mean", "mean_upper")
attr(sp_eff_wide, "raster") <- attr(pi_sp_eff_wide, "raster")
sp_eff_raster <- predict_to_raster(sp_eff_wide)

# save
stars::write_stars(merge(sp_eff_raster[-(1:2)]), file.path(path_out_aux, "eff_spatial.tif"))
```

## Simple visualization with stars

```{r}
# odds
ggplot() +
    stars::geom_stars(data = merge(sp_eff_raster[3:5])) +
    facet_wrap(~X1) +
    coord_sf(crs = st_crs(ama_study)) +
    colorspace::scale_fill_continuous_diverging(
        palette = "Purple-Green", l1 = 30, p1 = 0.9, p2 = 1, rev = TRUE, trans = "log")

#mean
ggplot() +
    stars::geom_stars(data = merge(sp_eff_raster[6:8])) +
    facet_wrap(~X1) +
    coord_sf(crs = st_crs(ama_study)) +
    colorspace::scale_fill_continuous_diverging(
        palette = "Purple-Green", l1 = 30, p1 = 0.9, p2 = 1)
```

## Time to execute the task

Only useful when executed with `Rscript`.

```{r}
proc.time()
```

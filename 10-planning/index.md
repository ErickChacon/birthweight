
# Birth weight study notes

## Problem Description

In this study we try to understand the effect of extreme events on the health on roadless cities on the Amazonas federal unit. Birthweight is used as a proxy of health.

## Literature Review

- [Birthweight as a proxy of health](birthweight-proxy.md)
- [Determinants of low birth weight](Determinants of low birth weight)
- [Measure of extreme events](Measure of extreme events)
- [The Amazonas population](The Amazonas population)

## Data Details

- Information System of Alive Newborns (SINASC)
- Tropical Rainfall Measuring Mission (TRMM)
- Census data

## Data Analysis

- [Birth Weight Exploratory](birthweight-exploratory.md)
- [Birth Weight Methodology](birthweight-methodology.md)
- [Birth Weight Modelling](birthweight-modelling.md)
- [Birth Weight Results](Birth Weight Results)

## [Planing](birthweight-planning.md)

## Nature sustainability

- [ ] update figures for paper (automatically)
- [ ] update tables for paper
- [ ] update quantities used in paper (e.g. effects, ci, etc)
- [ ] model with extreme floods and droughts by mckee
    - [ ] check `neg_ext_mckee` vs `pos_ext_mckee`
